package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.HandleStepConfig;

/**
 * 处理步骤配置Mapper接口
 * 
 * @author wind
 * @date 2022-03-21
 */
public interface HandleStepConfigMapper 
{
    /**
     * 查询处理步骤配置
     * 
     * @param id 处理步骤配置主键
     * @return 处理步骤配置
     */
    public HandleStepConfig selectHandleStepConfigById(Long id);

    /**
     * 查询处理步骤配置列表
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 处理步骤配置集合
     */
    public List<HandleStepConfig> selectHandleStepConfigList(HandleStepConfig handleStepConfig);

    /**
     * 新增处理步骤配置
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 结果
     */
    public int insertHandleStepConfig(HandleStepConfig handleStepConfig);

    /**
     * 修改处理步骤配置
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 结果
     */
    public int updateHandleStepConfig(HandleStepConfig handleStepConfig);

    /**
     * 删除处理步骤配置
     * 
     * @param id 处理步骤配置主键
     * @return 结果
     */
    public int deleteHandleStepConfigById(Long id);

    /**
     * 批量删除处理步骤配置
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHandleStepConfigByIds(Long[] ids);

    /**
     * 通过形态查询处理步骤配置
     * @param state
     * @return
     */
    public HandleStepConfig selectHandleStepConfigByState(String state);
}
