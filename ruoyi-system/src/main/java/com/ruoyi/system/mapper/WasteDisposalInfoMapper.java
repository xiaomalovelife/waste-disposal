package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.WasteDisposalInfo;

/**
 * 危险废物处置信息Mapper接口
 * 
 * @author wind
 * @date 2022-03-19
 */
public interface WasteDisposalInfoMapper 
{
    /**
     * 查询危险废物处置信息
     * 
     * @param id 危险废物处置信息主键
     * @return 危险废物处置信息
     */
    public WasteDisposalInfo selectWasteDisposalInfoById(Long id);

    /**
     * 查询危险废物处置信息列表
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 危险废物处置信息集合
     */
    public List<WasteDisposalInfo> selectWasteDisposalInfoList(WasteDisposalInfo wasteDisposalInfo);

    /**
     * 新增危险废物处置信息
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 结果
     */
    public int insertWasteDisposalInfo(WasteDisposalInfo wasteDisposalInfo);

    /**
     * 修改危险废物处置信息
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 结果
     */
    public int updateWasteDisposalInfo(WasteDisposalInfo wasteDisposalInfo);

    /**
     * 删除危险废物处置信息
     * 
     * @param id 危险废物处置信息主键
     * @return 结果
     */
    public int deleteWasteDisposalInfoById(Long id);

    /**
     * 批量删除危险废物处置信息
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWasteDisposalInfoByIds(Long[] ids);
}
