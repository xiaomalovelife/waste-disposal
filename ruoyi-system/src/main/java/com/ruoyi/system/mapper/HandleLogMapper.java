package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.HandleLog;

/**
 * 处理记录Mapper接口
 * 
 * @author wind
 * @date 2022-03-22
 */
public interface HandleLogMapper 
{
    /**
     * 查询处理记录
     * 
     * @param id 处理记录主键
     * @return 处理记录
     */
    public HandleLog selectHandleLogById(Long id);

    /**
     * 查询处理记录列表
     * 
     * @param handleLog 处理记录
     * @return 处理记录集合
     */
    public List<HandleLog> selectHandleLogList(HandleLog handleLog);

    /**
     * 新增处理记录
     * 
     * @param handleLog 处理记录
     * @return 结果
     */
    public int insertHandleLog(HandleLog handleLog);

    /**
     * 修改处理记录
     * 
     * @param handleLog 处理记录
     * @return 结果
     */
    public int updateHandleLog(HandleLog handleLog);

    /**
     * 删除处理记录
     * 
     * @param id 处理记录主键
     * @return 结果
     */
    public int deleteHandleLogById(Long id);

    /**
     * 批量删除处理记录
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteHandleLogByIds(Long[] ids);
}
