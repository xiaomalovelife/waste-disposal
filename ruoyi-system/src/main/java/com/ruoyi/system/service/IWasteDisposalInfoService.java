package com.ruoyi.system.service;

import java.util.List;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.system.domain.WasteDisposalInfo;

/**
 * 危险废物处置信息Service接口
 * 
 * @author wind
 * @date 2022-03-19
 */
public interface IWasteDisposalInfoService 
{
    /**
     * 查询危险废物处置信息
     * 
     * @param id 危险废物处置信息主键
     * @return 危险废物处置信息
     */
    public WasteDisposalInfo selectWasteDisposalInfoById(Long id);

    /**
     * 查询危险废物处置信息列表
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 危险废物处置信息集合
     */
    public List<WasteDisposalInfo> selectWasteDisposalInfoList(WasteDisposalInfo wasteDisposalInfo);

    /**
     * 新增危险废物处置信息
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 结果
     */
    public int insertWasteDisposalInfo(WasteDisposalInfo wasteDisposalInfo);

    /**
     * 修改危险废物处置信息
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 结果
     */
    public int updateWasteDisposalInfo(WasteDisposalInfo wasteDisposalInfo);

    /**
     * 批量删除危险废物处置信息
     * 
     * @param ids 需要删除的危险废物处置信息主键集合
     * @return 结果
     */
    public int deleteWasteDisposalInfoByIds(Long[] ids);

    /**
     * 删除危险废物处置信息信息
     * 
     * @param id 危险废物处置信息主键
     * @return 结果
     */
    public int deleteWasteDisposalInfoById(Long id);

    /**
     * 危险物品处理
     * @param id 危险废物处置信息主键
     * @return 结果
     */
    public int handleWaste(Long id, SysUser createUser);

    /**
     * 上报
     * @param id
     * @return
     */
    public int report(Long id);

    /**
     * 通报
     * @param id
     * @param notifyType
     * @return
     */
    public int notify(Long id,String notifyType,SysUser createUser);
}
