package com.ruoyi.system.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import com.ruoyi.common.core.domain.entity.SysUser;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.system.domain.HandleLog;
import com.ruoyi.system.domain.SysNotice;
import com.ruoyi.system.domain.vo.HandleStepVO;
import com.ruoyi.system.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.annotation.Transient;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.WasteDisposalInfoMapper;
import com.ruoyi.system.domain.WasteDisposalInfo;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;

/**
 * 危险废物处置信息Service业务层处理
 * 
 * @author wind
 * @date 2022-03-19
 */
@Service
public class WasteDisposalInfoServiceImpl implements IWasteDisposalInfoService 
{
    @Resource
    private WasteDisposalInfoMapper wasteDisposalInfoMapper;
    @Resource
    private IHandleStepConfigService handleStepConfigService;
    @Resource
    private IHandleLogService handleLogService;
    @Resource
    private ISysNoticeService sysNoticeService;
    @Resource
    private ISysDictDataService sysDictDataService;

    /**
     * 查询危险废物处置信息
     * 
     * @param id 危险废物处置信息主键
     * @return 危险废物处置信息
     */
    @Override
    public WasteDisposalInfo selectWasteDisposalInfoById(Long id)
    {
        return wasteDisposalInfoMapper.selectWasteDisposalInfoById(id);
    }

    /**
     * 查询危险废物处置信息列表
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 危险废物处置信息
     */
    @Override
    public List<WasteDisposalInfo> selectWasteDisposalInfoList(WasteDisposalInfo wasteDisposalInfo)
    {
        return wasteDisposalInfoMapper.selectWasteDisposalInfoList(wasteDisposalInfo);
    }

    /**
     * 新增危险废物处置信息
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 结果
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public int insertWasteDisposalInfo(WasteDisposalInfo wasteDisposalInfo)
    {
        List<HandleStepVO> step = handleStepConfigService.selectHandleStepConfigByState(wasteDisposalInfo.getState());
        if(CollectionUtils.isEmpty(step)){
            throw new ServiceException("此形态下未配置处理步骤,请等待处置中心配置！");
        }
        //判重
        WasteDisposalInfo queryWaste = new WasteDisposalInfo();
        queryWaste.setPlan(wasteDisposalInfo.getPlan());
        queryWaste.setMonthly(wasteDisposalInfo.getMonthly());
        queryWaste.setState(wasteDisposalInfo.getState());
        List<WasteDisposalInfo> query = this.selectWasteDisposalInfoList(queryWaste);
        if(!CollectionUtils.isEmpty(query)){
            throw new ServiceException("此形态下月度上报已存在,请无重复添加！");
        }
        HandleStepVO handleStepVO =  step.stream().filter(item -> item.getStep()==1).collect(Collectors.toList()).get(0);
        //默认未上报
        wasteDisposalInfo.setReport("0");
        //默认未处理完成
        wasteDisposalInfo.setComplete("0");
        wasteDisposalInfo.setNotifyType("0");
        wasteDisposalInfo.setHandleDeptId(handleStepVO.getDeptId());
        wasteDisposalInfo.setHandleStep(handleStepVO.getStep());
        wasteDisposalInfo.setCreateTime(DateUtils.getNowDate());
        return wasteDisposalInfoMapper.insertWasteDisposalInfo(wasteDisposalInfo);
    }

    /**
     * 修改危险废物处置信息
     * 
     * @param wasteDisposalInfo 危险废物处置信息
     * @return 结果
     */
    @Override
    public int updateWasteDisposalInfo(WasteDisposalInfo wasteDisposalInfo)
    {
        List<HandleStepVO> step = handleStepConfigService.selectHandleStepConfigByState(wasteDisposalInfo.getState());
        if(CollectionUtils.isEmpty(step)){
            throw new ServiceException("此形态下未配置处理步骤,请等待处置中心配置！");
        }
        //判重
        WasteDisposalInfo queryWaste = new WasteDisposalInfo();
        queryWaste.setPlan(wasteDisposalInfo.getPlan());
        queryWaste.setMonthly(wasteDisposalInfo.getMonthly());
        queryWaste.setState(wasteDisposalInfo.getState());
        List<WasteDisposalInfo> query = this.selectWasteDisposalInfoList(queryWaste);
        query = query.stream().filter(item -> !item.getId().equals(wasteDisposalInfo.getId())).collect(Collectors.toList());
        if(!CollectionUtils.isEmpty(query)){
            throw new ServiceException("此形态下月度上报已存在,请无重复添加！");
        }
        wasteDisposalInfo.setUpdateTime(DateUtils.getNowDate());
        return wasteDisposalInfoMapper.updateWasteDisposalInfo(wasteDisposalInfo);
    }

    /**
     * 批量删除危险废物处置信息
     * 
     * @param ids 需要删除的危险废物处置信息主键
     * @return 结果
     */
    @Override
    public int deleteWasteDisposalInfoByIds(Long[] ids)
    {
        return wasteDisposalInfoMapper.deleteWasteDisposalInfoByIds(ids);
    }

    /**
     * 删除危险废物处置信息信息
     * 
     * @param id 危险废物处置信息主键
     * @return 结果
     */
    @Override
    public int deleteWasteDisposalInfoById(Long id)
    {
        return wasteDisposalInfoMapper.deleteWasteDisposalInfoById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int handleWaste(Long id , SysUser createUser) {
        //通过id查询危险废物信息
        WasteDisposalInfo wasteDisposalInfo = this.selectWasteDisposalInfoById(id);
        //危险废物形态
        String state = wasteDisposalInfo.getState();
        //当前处理步骤
        int step = wasteDisposalInfo.getHandleStep();
        //通过形态查询处理步骤
        List<HandleStepVO> stepList = handleStepConfigService.selectHandleStepConfigByState(state);
        //当前步骤加1及下一步
        int nextStep = step+1;
        SysNotice notice = new SysNotice();
        String stateTxt = sysDictDataService.selectDictLabel("dict_wastes_state",state);
        //处理步骤条数即为最大就是最后一步
        //判断下一步大于最后一步即为完成 将状态置为完成，发送通知
        if(nextStep>stepList.size()){
            wasteDisposalInfo.setComplete("1");
            SysNotice noticeComplete = new SysNotice();
            noticeComplete.setNoticeTitle(wasteDisposalInfo.getMonthly()+"月度"+stateTxt+"废物处理全部完成");
            noticeComplete.setStatus("0");
            noticeComplete.setNoticeType("1");
            noticeComplete.setCreateDeptId(createUser.getDeptId());
            noticeComplete.setCreateDeptName(createUser.getDept().getDeptName());
            noticeComplete.setCreateBy(createUser.getUserName());
            noticeComplete.setReceiveDeptId(wasteDisposalInfo.getCreateDeptId());
            noticeComplete.setReceiveDeptName(wasteDisposalInfo.getCreateDeptName());
            sysNoticeService.insertNotice(noticeComplete);
        }else { //其他情况正常步骤加1，查询处理部门
            wasteDisposalInfo.setHandleStep(nextStep);
            //筛选步骤
            HandleStepVO handleStepVO = stepList.stream().filter(item -> item.getStep() ==nextStep).collect(Collectors.toList()).get(0);
            wasteDisposalInfo.setHandleDeptId(handleStepVO.getDeptId());
        }
        //添加处理记录
        HandleLog handleLog = new HandleLog();
        handleLog.setWasteId(wasteDisposalInfo.getId());
        handleLog.setStep(step);
        handleLog.setCreateDeptId(createUser.getDeptId());
        handleLog.setCreateDeptName(createUser.getDept().getDeptName());
        handleLog.setCreateUserPhone(createUser.getPhonenumber());
        handleLog.setCreateBy(createUser.getUserName());
        handleLogService.insertHandleLog(handleLog);
        notice.setNoticeTitle(wasteDisposalInfo.getMonthly()+"月度"+stateTxt+"废物第"+step +"步处理完成");
        //发送通知
        notice.setStatus("0");
        notice.setNoticeType("1");
        notice.setCreateDeptId(createUser.getDeptId());
        notice.setCreateDeptName(createUser.getDept().getDeptName());
        notice.setCreateBy(createUser.getUserName());
        notice.setReceiveDeptId(wasteDisposalInfo.getCreateDeptId());
        notice.setReceiveDeptName(wasteDisposalInfo.getCreateDeptName());
        sysNoticeService.insertNotice(notice);
        //修改更新
        return wasteDisposalInfoMapper.updateWasteDisposalInfo(wasteDisposalInfo);
    }

    @Override
    public int report(Long id) {
        //通过id查询危险废物信息
        WasteDisposalInfo wasteDisposalInfo = this.selectWasteDisposalInfoById(id);
        wasteDisposalInfo.setReport("1");
        return wasteDisposalInfoMapper.updateWasteDisposalInfo(wasteDisposalInfo);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int notify(Long id, String notifyType,SysUser createUser) {
        //更改状态
        WasteDisposalInfo wasteDisposalInfo = this.selectWasteDisposalInfoById(id);
        String state = wasteDisposalInfo.getState();
        String stateTxt = sysDictDataService.selectDictLabel("dict_wastes_state",state);
        wasteDisposalInfo.setNotifyType(notifyType);
        //发送通知
        SysNotice notice = new SysNotice();
        String notifyTypeTxt = "";
        if(StringUtils.equals("1",notifyType)){
            notifyTypeTxt = "通过";
        }else if(StringUtils.equals("2",notifyType)){
            notifyTypeTxt = "警告";
        }
        notice.setNoticeTitle(wasteDisposalInfo.getMonthly()+"月度"+stateTxt+"废物处理"+notifyTypeTxt);
        notice.setStatus("0");
        notice.setNoticeType(notifyType);
        notice.setCreateDeptId(createUser.getDeptId());
        notice.setCreateDeptName(createUser.getDept().getDeptName());
        notice.setCreateBy(createUser.getUserName());
        notice.setReceiveDeptId(wasteDisposalInfo.getCreateDeptId());
        notice.setReceiveDeptName(wasteDisposalInfo.getCreateDeptName());
        sysNoticeService.insertNotice(notice);
        return wasteDisposalInfoMapper.updateWasteDisposalInfo(wasteDisposalInfo);
    }
}
