package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.HandleStepConfig;
import com.ruoyi.system.domain.vo.HandleStepVO;

/**
 * 处理步骤配置Service接口
 * 
 * @author wind
 * @date 2022-03-21
 */
public interface IHandleStepConfigService 
{
    /**
     * 查询处理步骤配置
     * 
     * @param id 处理步骤配置主键
     * @return 处理步骤配置
     */
    public HandleStepConfig selectHandleStepConfigById(Long id);

    /**
     * 查询处理步骤配置列表
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 处理步骤配置集合
     */
    public List<HandleStepConfig> selectHandleStepConfigList(HandleStepConfig handleStepConfig);

    /**
     * 新增处理步骤配置
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 结果
     */
    public int insertHandleStepConfig(HandleStepConfig handleStepConfig);

    /**
     * 修改处理步骤配置
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 结果
     */
    public int updateHandleStepConfig(HandleStepConfig handleStepConfig);

    /**
     * 批量删除处理步骤配置
     * 
     * @param ids 需要删除的处理步骤配置主键集合
     * @return 结果
     */
    public int deleteHandleStepConfigByIds(Long[] ids);

    /**
     * 删除处理步骤配置信息
     * 
     * @param id 处理步骤配置主键
     * @return 结果
     */
    public int deleteHandleStepConfigById(Long id);

    /**
     * 查询某状态下步骤配置
     * @param state 形态
     * @return
     */
    public List<HandleStepVO> selectHandleStepConfigByState(String state);
}
