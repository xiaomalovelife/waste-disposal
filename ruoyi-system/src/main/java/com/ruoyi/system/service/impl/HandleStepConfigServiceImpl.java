package com.ruoyi.system.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.common.exception.ServiceException;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.HandleStepConfig;
import com.ruoyi.system.domain.vo.HandleStepVO;
import com.ruoyi.system.mapper.HandleStepConfigMapper;
import com.ruoyi.system.service.IHandleStepConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * 处理步骤配置Service业务层处理
 * 
 * @author wind
 * @date 2022-03-21
 */
@Service
public class HandleStepConfigServiceImpl implements IHandleStepConfigService 
{
    @Resource
    private HandleStepConfigMapper handleStepConfigMapper;

    /**
     * 查询处理步骤配置
     * 
     * @param id 处理步骤配置主键
     * @return 处理步骤配置
     */
    @Override
    public HandleStepConfig selectHandleStepConfigById(Long id)
    {
        HandleStepConfig handleStepConfig =  handleStepConfigMapper.selectHandleStepConfigById(id);
        //将步骤JSONString转为List
        handleStepConfig.setHandleStepList(JSONArray.parseArray(handleStepConfig.getConfig(), HandleStepVO.class));
        return handleStepConfig;
    }

    /**
     * 查询处理步骤配置列表
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 处理步骤配置
     */
    @Override
    public List<HandleStepConfig> selectHandleStepConfigList(HandleStepConfig handleStepConfig)
    {
        return handleStepConfigMapper.selectHandleStepConfigList(handleStepConfig);
    }

    /**
     * 新增处理步骤配置
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 结果
     */
    @Override
    public int insertHandleStepConfig(HandleStepConfig handleStepConfig)
    {
        //以形态查询已配置步骤，保证各形态处理步骤唯一
        HandleStepConfig queryHandleStepConfig = new HandleStepConfig();
        queryHandleStepConfig.setState(handleStepConfig.getState());
        List query =this.selectHandleStepConfigList(queryHandleStepConfig);
        if(!CollectionUtils.isEmpty(query)){
            throw new ServiceException("此形态的配置已存在，请勿重复添加！");
        }
        //将步骤List以JSONString入库
        handleStepConfig.setConfig(CollectionUtils.isEmpty(handleStepConfig.getHandleStepList())? null : JSONArray.toJSONString(handleStepConfig.getHandleStepList()));
        //创建时间
        handleStepConfig.setCreateTime(DateUtils.getNowDate());
        return handleStepConfigMapper.insertHandleStepConfig(handleStepConfig);
    }

    /**
     * 修改处理步骤配置
     * 
     * @param handleStepConfig 处理步骤配置
     * @return 结果
     */
    @Override
    public int updateHandleStepConfig(HandleStepConfig handleStepConfig)
    {
        //将步骤List以JSONString入库
        handleStepConfig.setConfig(CollectionUtils.isEmpty(handleStepConfig.getHandleStepList())? null : JSONArray.toJSONString(handleStepConfig.getHandleStepList()));
        //创建时间
        handleStepConfig.setUpdateTime(DateUtils.getNowDate());
        return handleStepConfigMapper.updateHandleStepConfig(handleStepConfig);
    }

    /**
     * 批量删除处理步骤配置
     * 
     * @param ids 需要删除的处理步骤配置主键
     * @return 结果
     */
    @Override
    public int deleteHandleStepConfigByIds(Long[] ids)
    {
        return handleStepConfigMapper.deleteHandleStepConfigByIds(ids);
    }

    /**
     * 删除处理步骤配置信息
     * 
     * @param id 处理步骤配置主键
     * @return 结果
     */
    @Override
    public int deleteHandleStepConfigById(Long id)
    {
        return handleStepConfigMapper.deleteHandleStepConfigById(id);
    }

    @Override
    public List<HandleStepVO> selectHandleStepConfigByState(String state) {
        HandleStepConfig handleStepConfig =  handleStepConfigMapper.selectHandleStepConfigByState(state);
        if(Objects.isNull(handleStepConfig)){
            return null;
        }else{//JSONString转List
            return JSONArray.parseArray(handleStepConfig.getConfig(), HandleStepVO.class);
        }
    }
}
