package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.HandleLogMapper;
import com.ruoyi.system.domain.HandleLog;
import com.ruoyi.system.service.IHandleLogService;

/**
 * 处理记录Service业务层处理
 * 
 * @author wind
 * @date 2022-03-22
 */
@Service
public class HandleLogServiceImpl implements IHandleLogService 
{
    @Autowired
    private HandleLogMapper handleLogMapper;

    /**
     * 查询处理记录
     * 
     * @param id 处理记录主键
     * @return 处理记录
     */
    @Override
    public HandleLog selectHandleLogById(Long id)
    {
        return handleLogMapper.selectHandleLogById(id);
    }

    /**
     * 查询处理记录列表
     * 
     * @param handleLog 处理记录
     * @return 处理记录
     */
    @Override
    public List<HandleLog> selectHandleLogList(HandleLog handleLog)
    {
        return handleLogMapper.selectHandleLogList(handleLog);
    }

    /**
     * 新增处理记录
     * 
     * @param handleLog 处理记录
     * @return 结果
     */
    @Override
    public int insertHandleLog(HandleLog handleLog)
    {
        handleLog.setCreateTime(DateUtils.getNowDate());
        return handleLogMapper.insertHandleLog(handleLog);
    }

    /**
     * 修改处理记录
     * 
     * @param handleLog 处理记录
     * @return 结果
     */
    @Override
    public int updateHandleLog(HandleLog handleLog)
    {
        return handleLogMapper.updateHandleLog(handleLog);
    }

    /**
     * 批量删除处理记录
     * 
     * @param ids 需要删除的处理记录主键
     * @return 结果
     */
    @Override
    public int deleteHandleLogByIds(Long[] ids)
    {
        return handleLogMapper.deleteHandleLogByIds(ids);
    }

    /**
     * 删除处理记录信息
     * 
     * @param id 处理记录主键
     * @return 结果
     */
    @Override
    public int deleteHandleLogById(Long id)
    {
        return handleLogMapper.deleteHandleLogById(id);
    }
}
