package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 处理记录对象 t_handle_log
 * 
 * @author wind
 * @date 2022-03-22
 */
public class HandleLog extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 废物处理id */
    @Excel(name = "废物处理id")
    private Long wasteId;

    /** 处理步骤 */
    @Excel(name = "处理步骤")
    private Integer step;

    /** 创建部门id */
    @Excel(name = "创建部门id")
    private Long createDeptId;

    /** 创建部门名称 */
    @Excel(name = "创建部门名称")
    private String createDeptName;

    /** 创建人电话 */
    @Excel(name = "创建人电话")
    private String createUserPhone;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setWasteId(Long wasteId) 
    {
        this.wasteId = wasteId;
    }

    public Long getWasteId() 
    {
        return wasteId;
    }
    public void setStep(Integer step) 
    {
        this.step = step;
    }

    public Integer getStep() 
    {
        return step;
    }
    public void setCreateDeptId(Long createDeptId) 
    {
        this.createDeptId = createDeptId;
    }

    public Long getCreateDeptId() 
    {
        return createDeptId;
    }
    public void setCreateDeptName(String createDeptName) 
    {
        this.createDeptName = createDeptName;
    }

    public String getCreateDeptName() 
    {
        return createDeptName;
    }
    public void setCreateUserPhone(String createUserPhone) 
    {
        this.createUserPhone = createUserPhone;
    }

    public String getCreateUserPhone() 
    {
        return createUserPhone;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("wasteId", getWasteId())
            .append("step", getStep())
            .append("createDeptId", getCreateDeptId())
            .append("createDeptName", getCreateDeptName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("createUserPhone", getCreateUserPhone())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
