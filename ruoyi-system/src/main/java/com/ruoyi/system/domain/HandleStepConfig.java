package com.ruoyi.system.domain;

import com.alibaba.fastjson.JSONArray;
import com.ruoyi.system.domain.vo.HandleStepVO;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

import java.util.List;

/**
 * 处理步骤配置对象 t_handle_step_config
 * 
 * @author wind
 * @date 2022-03-21
 */
public class HandleStepConfig extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 形态 */
    @Excel(name = "形态")
    private String state;

    /** 配置 */
    @Excel(name = "配置")
    private String config;

    /** 配置列表 */
    private List<HandleStepVO> handleStepList;

    /** 创建部门id */
    @Excel(name = "创建部门id")
    private Long createDeptId;

    /** 创建部门名称 */
    @Excel(name = "创建部门名称")
    private String createDeptName;

    /** 创建人电话 */
    @Excel(name = "创建人电话")
    private String createUserPhone;

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setConfig(String config)
    {
        this.config = config;
    }

    public String getConfig()
    {
        return config;
    }
    public void setCreateDeptId(Long createDeptId) 
    {
        this.createDeptId = createDeptId;
    }

    public Long getCreateDeptId() 
    {
        return createDeptId;
    }
    public void setCreateDeptName(String createDeptName) 
    {
        this.createDeptName = createDeptName;
    }

    public String getCreateDeptName() 
    {
        return createDeptName;
    }
    public void setCreateUserPhone(String createUserPhone) 
    {
        this.createUserPhone = createUserPhone;
    }

    public String getCreateUserPhone() 
    {
        return createUserPhone;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public List<HandleStepVO> getHandleStepList() {
        return this.handleStepList;
    }

    public void setHandleStepList(List<HandleStepVO> handleStepList) {
        this.handleStepList = handleStepList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("state", getState())
            .append("config", getConfig())
            .append("createDeptId", getCreateDeptId())
            .append("createDeptName", getCreateDeptName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("createUserPhone", getCreateUserPhone())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
