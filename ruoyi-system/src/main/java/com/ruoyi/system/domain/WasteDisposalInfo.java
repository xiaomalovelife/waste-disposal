package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 危险废物处置信息对象 t_waste_disposal_info
 * 
 * @author wind
 * @date 2022-03-19
 */
public class WasteDisposalInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 名称 */
    @Excel(name = "名称")
    private String name;

    /** 数量 */
    @Excel(name = "数量")
    private String num;

    /** 单位 */
    @Excel(name = "单位")
    private String unit;

    /** 是否计划 */
    @Excel(name = "是否计划")
    private String plan;

    /** 形态 */
    @Excel(name = "形态")
    private String state;

    /** 月份 */
    @Excel(name = "月份")
    private String monthly;

    /** 是否完成处理 */
    @Excel(name = "是否完成处理")
    private String complete;

    /** 上报状态 */
    @Excel(name = "上报状态")
    private String report;

    private Long createById;

    /** 创建部门id */
    @Excel(name = "创建部门id")
    private Long createDeptId;

    /** 创建部门名称 */
    @Excel(name = "创建部门名称")
    private String createDeptName;

    /** 创建人电话 */
    @Excel(name = "创建人电话")
    private String createUserPhone;

    /** 当前处理步骤 */
    @Excel(name = "当前处理步骤")
    private Integer handleStep;

    /** 当前处理部门id */
    @Excel(name = "当前处理部门id")
    private Long handleDeptId;

    private String notifyType;

    public String getNotifyType() {
        return notifyType;
    }

    public void setNotifyType(String notifyType) {
        this.notifyType = notifyType;
    }

    /** 删除标志（0代表存在 2代表删除） */
    private String delFlag;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setNum(String num) 
    {
        this.num = num;
    }

    public String getNum() 
    {
        return num;
    }
    public void setUnit(String unit) 
    {
        this.unit = unit;
    }

    public String getUnit() 
    {
        return unit;
    }
    public void setPlan(String plan) 
    {
        this.plan = plan;
    }

    public String getPlan() 
    {
        return plan;
    }
    public void setState(String state) 
    {
        this.state = state;
    }

    public String getState() 
    {
        return state;
    }
    public void setMonthly(String monthly) 
    {
        this.monthly = monthly;
    }

    public String getMonthly() 
    {
        return monthly;
    }
    public void setComplete(String complete) 
    {
        this.complete = complete;
    }

    public String getComplete() 
    {
        return complete;
    }
    public void setReport(String report) 
    {
        this.report = report;
    }

    public String getReport() 
    {
        return report;
    }
    public void setCreateDeptId(Long createDeptId) 
    {
        this.createDeptId = createDeptId;
    }

    public Long getCreateDeptId() 
    {
        return createDeptId;
    }
    public void setCreateDeptName(String createDeptName) 
    {
        this.createDeptName = createDeptName;
    }

    public String getCreateDeptName()
    {
        return createDeptName;
    }

    public void setCreateUserPhone(String createUserPhone)
    {
        this.createUserPhone = createUserPhone;
    }

    public String getCreateUserPhone()
    {
        return createUserPhone;
    }
    public void setHandleStep(Integer handleStep) 
    {
        this.handleStep = handleStep;
    }

    public Integer getHandleStep() 
    {
        return handleStep;
    }
    public void setHandleDeptId(Long handleDeptId) 
    {
        this.handleDeptId = handleDeptId;
    }

    public Long getHandleDeptId() 
    {
        return handleDeptId;
    }
    public void setDelFlag(String delFlag) 
    {
        this.delFlag = delFlag;
    }

    public String getDelFlag() 
    {
        return delFlag;
    }

    public Long getCreateById() {
        return createById;
    }

    public void setCreateById(Long createById) {
        this.createById = createById;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("num", getNum())
            .append("unit", getUnit())
            .append("plan", getPlan())
            .append("state", getState())
            .append("monthly", getMonthly())
            .append("complete", getComplete())
            .append("report", getReport())
            .append("createDeptId", getCreateDeptId())
            .append("createDeptName", getCreateDeptName())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("createUserPhone", getCreateUserPhone())
            .append("handleStep", getHandleStep())
            .append("handleDeptId", getHandleDeptId())
            .append("delFlag", getDelFlag())
            .toString();
    }
}
