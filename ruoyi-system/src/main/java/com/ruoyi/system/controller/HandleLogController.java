package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.HandleLog;
import com.ruoyi.system.service.IHandleLogService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 处理记录Controller
 * 
 * @author wind
 * @date 2022-03-22
 */
@RestController
@RequestMapping("/handleLog")
public class HandleLogController extends BaseController
{
    @Autowired
    private IHandleLogService handleLogService;

    /**
     * 查询处理记录列表
     */
    @PreAuthorize("@ss.hasPermi('handle:log:list')")
    @GetMapping("/list")
    public List<HandleLog> list(HandleLog handleLog)
    {
        return handleLogService.selectHandleLogList(handleLog);
    }

    /**
     * 导出处理记录列表
     */
    @PreAuthorize("@ss.hasPermi('handle:log:export')")
    @Log(title = "处理记录", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, HandleLog handleLog)
    {
        List<HandleLog> list = handleLogService.selectHandleLogList(handleLog);
        ExcelUtil<HandleLog> util = new ExcelUtil<HandleLog>(HandleLog.class);
        util.exportExcel(response, list, "处理记录数据");
    }

    /**
     * 获取处理记录详细信息
     */
    @PreAuthorize("@ss.hasPermi('handle:log:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(handleLogService.selectHandleLogById(id));
    }

    /**
     * 新增处理记录
     */
    @PreAuthorize("@ss.hasPermi('handle:log:add')")
    @Log(title = "处理记录", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HandleLog handleLog)
    {
        return toAjax(handleLogService.insertHandleLog(handleLog));
    }

    /**
     * 修改处理记录
     */
    @PreAuthorize("@ss.hasPermi('handle:log:edit')")
    @Log(title = "处理记录", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HandleLog handleLog)
    {
        return toAjax(handleLogService.updateHandleLog(handleLog));
    }

    /**
     * 删除处理记录
     */
    @PreAuthorize("@ss.hasPermi('handle:log:remove')")
    @Log(title = "处理记录", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(handleLogService.deleteHandleLogByIds(ids));
    }
}
