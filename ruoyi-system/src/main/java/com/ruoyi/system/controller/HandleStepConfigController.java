package com.ruoyi.system.controller;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.HandleStepConfig;
import com.ruoyi.system.service.IHandleStepConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 处理步骤配置Controller
 * 
 * @author wind
 * @date 2022-03-21
 */
@RestController
@RequestMapping("/handleStep")
public class HandleStepConfigController extends BaseController
{
    @Autowired
    private IHandleStepConfigService handleStepConfigService;

    /**
     * 查询处理步骤配置列表
     */
    @PreAuthorize("@ss.hasPermi('handleStep:config:list')")
    @GetMapping("/list")
    public TableDataInfo list(HandleStepConfig handleStepConfig)
    {
        startPage();
        List<HandleStepConfig> list = handleStepConfigService.selectHandleStepConfigList(handleStepConfig);
        return getDataTable(list);
    }

    /**
     * 导出处理步骤配置列表
     */
    @PreAuthorize("@ss.hasPermi('handleStep:config:export')")
    @Log(title = "处理步骤配置", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, HandleStepConfig handleStepConfig)
    {
        List<HandleStepConfig> list = handleStepConfigService.selectHandleStepConfigList(handleStepConfig);
        ExcelUtil<HandleStepConfig> util = new ExcelUtil<HandleStepConfig>(HandleStepConfig.class);
        util.exportExcel(response, list, "处理步骤配置数据");
    }

    /**
     * 获取处理步骤配置详细信息
     */
    @PreAuthorize("@ss.hasPermi('handleStep:config:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(handleStepConfigService.selectHandleStepConfigById(id));
    }

    /**
     * 新增处理步骤配置
     */
    @PreAuthorize("@ss.hasPermi('handleStep:config:add')")
    @Log(title = "处理步骤配置", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody HandleStepConfig handleStepConfig)
    {
        handleStepConfig.setCreateDeptId(getDeptId());
        handleStepConfig.setCreateBy(getUsername());
        handleStepConfig.setCreateDeptName(getLoginUser().getUser().getDept().getDeptName());
        handleStepConfig.setCreateUserPhone(getLoginUser().getUser().getPhonenumber());
        return toAjax(handleStepConfigService.insertHandleStepConfig(handleStepConfig));
    }

    /**
     * 修改处理步骤配置
     */
    @PreAuthorize("@ss.hasPermi('handleStep:config:edit')")
    @Log(title = "处理步骤配置", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody HandleStepConfig handleStepConfig)
    {
        handleStepConfig.setCreateDeptId(getDeptId());
        handleStepConfig.setUpdateBy(getUsername());
        return toAjax(handleStepConfigService.updateHandleStepConfig(handleStepConfig));
    }

    /**
     * 删除处理步骤配置
     */
    @PreAuthorize("@ss.hasPermi('handleStep:config:remove')")
    @Log(title = "处理步骤配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(handleStepConfigService.deleteHandleStepConfigByIds(ids));
    }
}
