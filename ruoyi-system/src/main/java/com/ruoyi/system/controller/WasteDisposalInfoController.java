package com.ruoyi.system.controller;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSON;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.WasteDisposalInfo;
import com.ruoyi.system.service.IWasteDisposalInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 危险废物处置信息Controller
 * 
 * @author wind
 * @date 2022-03-19
 */
@RestController
@RequestMapping("/wasteDisposal/info")
public class WasteDisposalInfoController extends BaseController
{
    @Autowired
    private IWasteDisposalInfoService wasteDisposalInfoService;

    /**
     * 查询危险废物处置信息列表
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(WasteDisposalInfo wasteDisposalInfo)
    {
        startPage();
        List<WasteDisposalInfo> list = wasteDisposalInfoService.selectWasteDisposalInfoList(wasteDisposalInfo);
        return getDataTable(list);
    }

    /**
     * 导出危险废物处置信息列表
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:export')")
    @Log(title = "危险废物处置信息", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, WasteDisposalInfo wasteDisposalInfo)
    {
        List<WasteDisposalInfo> list = wasteDisposalInfoService.selectWasteDisposalInfoList(wasteDisposalInfo);
        ExcelUtil<WasteDisposalInfo> util = new ExcelUtil<WasteDisposalInfo>(WasteDisposalInfo.class);
        util.exportExcel(response, list, "危险废物处置信息数据");
    }

    /**
     * 获取危险废物处置信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(wasteDisposalInfoService.selectWasteDisposalInfoById(id));
    }

    /**
     * 新增危险废物处置信息
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:add')")
    @Log(title = "危险废物处置信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WasteDisposalInfo wasteDisposalInfo)
    {
        wasteDisposalInfo.setCreateDeptId(getDeptId());
        wasteDisposalInfo.setCreateBy(getUsername());
        wasteDisposalInfo.setCreateById(getUserId());
        wasteDisposalInfo.setCreateDeptName(getLoginUser().getUser().getDept().getDeptName());
        wasteDisposalInfo.setCreateUserPhone(getLoginUser().getUser().getPhonenumber());
        return toAjax(wasteDisposalInfoService.insertWasteDisposalInfo(wasteDisposalInfo));
    }

    /**
     * 修改危险废物处置信息
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:edit')")
    @Log(title = "危险废物处置信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WasteDisposalInfo wasteDisposalInfo)
    {
        wasteDisposalInfo.setUpdateBy(getUsername());
        return toAjax(wasteDisposalInfoService.updateWasteDisposalInfo(wasteDisposalInfo));
    }

    /**
     * 删除危险废物处置信息
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:remove')")
    @Log(title = "危险废物处置信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wasteDisposalInfoService.deleteWasteDisposalInfoByIds(ids));
    }


    /**
     * 处理
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:handleWaste')")
    @Log(title = "危险废物处置信息", businessType = BusinessType.UPDATE)
    @PutMapping("/handleWaste/{id}")
    public AjaxResult handleWaste(@PathVariable Long id)
    {
        return toAjax(wasteDisposalInfoService.handleWaste(id, getLoginUser().getUser()));
    }

    /**
     * 上报
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:report')")
    @Log(title = "危险废物处置信息", businessType = BusinessType.UPDATE)
    @PutMapping("/report/{id}")
    public AjaxResult report(@PathVariable Long id)
    {
        return toAjax(wasteDisposalInfoService.report(id));
    }

    /**
     * 比对查询
     * @param id
     * @return
     */
    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:compareList')")
    @GetMapping("/compareList/{id}")
    public List<WasteDisposalInfo> compareList(@PathVariable Long id)
    {
        WasteDisposalInfo wasteDisposalInfo = wasteDisposalInfoService.selectWasteDisposalInfoById(id);
        WasteDisposalInfo query = new WasteDisposalInfo();
        query.setCreateBy(wasteDisposalInfo.getCreateBy());
        query.setState(wasteDisposalInfo.getState());
        query.setMonthly(wasteDisposalInfo.getMonthly());
        List<WasteDisposalInfo> list = wasteDisposalInfoService.selectWasteDisposalInfoList(query);
        return list.stream().sorted(Comparator.comparing(WasteDisposalInfo::getPlan)).collect(Collectors.toList());
    }

    @PreAuthorize("@ss.hasPermi('wasteDisposal:info:compareList')")
    @Log(title = "危险废物处置信息", businessType = BusinessType.UPDATE)
    @PostMapping("/notify")
    public AjaxResult notify(@RequestBody Map request)
    {
        return toAjax(wasteDisposalInfoService.notify(Long.parseLong(request.get("id").toString()),request.get("notifyType").toString(),getLoginUser().getUser()));
    }
}
