import request from '@/utils/request'

// 查询处理步骤配置列表
export function listConfig(query) {
  return request({
    url: '/handleStep/list',
    method: 'get',
    params: query
  })
}

// 查询处理步骤配置详细
export function getConfig(id) {
  return request({
    url: '/handleStep/' + id,
    method: 'get'
  })
}

// 新增处理步骤配置
export function addConfig(data) {
  return request({
    url: '/handleStep',
    method: 'post',
    data: data
  })
}

// 修改处理步骤配置
export function updateConfig(data) {
  return request({
    url: '/handleStep',
    method: 'put',
    data: data
  })
}

// 删除处理步骤配置
export function delConfig(id) {
  return request({
    url: '/handleStep/' + id,
    method: 'delete'
  })
}
