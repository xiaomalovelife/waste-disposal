import request from '@/utils/request'

// 查询危险废物处置信息列表
export function listInfo(query) {
  return request({
    url: '/wasteDisposal/info/list',
    method: 'get',
    params: query
  })
}

// 查询危险废物处置信息详细
export function getInfo(id) {
  return request({
    url: '/wasteDisposal/info/' + id,
    method: 'get'
  })
}

// 新增危险废物处置信息
export function addInfo(data) {
  return request({
    url: '/wasteDisposal/info',
    method: 'post',
    data: data
  })
}

// 修改危险废物处置信息
export function updateInfo(data) {
  return request({
    url: '/wasteDisposal/info',
    method: 'put',
    data: data
  })
}

// 删除危险废物处置信息
export function delInfo(id) {
  return request({
    url: '/wasteDisposal/info/' + id,
    method: 'delete'
  })
}

// 危险废物处置
export function handleWaste(id) {
  return request({
    url: '/wasteDisposal/info/handleWaste/' + id,
    method: 'put'
  })
}

// 危险废物上报
export function report(id) {
  return request({
    url: '/wasteDisposal/info/report/' + id,
    method: 'put'
  })
}

// 查询危险废物处置信息比对列表
export function compareList(id) {
  return request({
    url: '/wasteDisposal/info/compareList/'+id,
    method: 'get'
  })
}

// 新增危险废物处置信息
export function notify(data) {
  return request({
    url: '/wasteDisposal/info/notify',
    method: 'post',
    data: data
  })
}
