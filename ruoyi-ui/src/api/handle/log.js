import request from '@/utils/request'

// 查询处理记录列表
export function listLog(query) {
  return request({
    url: '/handleLog/list',
    method: 'get',
    params: query
  })
}

// 查询处理记录详细
export function getLog(id) {
  return request({
    url: '/handleLog/' + id,
    method: 'get'
  })
}

// 新增处理记录
export function addLog(data) {
  return request({
    url: '/handleLog',
    method: 'post',
    data: data
  })
}

// 修改处理记录
export function updateLog(data) {
  return request({
    url: '/handleLog',
    method: 'put',
    data: data
  })
}

// 删除处理记录
export function delLog(id) {
  return request({
    url: '/handleLog/' + id,
    method: 'delete'
  })
}
