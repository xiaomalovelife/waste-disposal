/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50722
Source Host           : localhost:3306
Source Database       : waste_disposal

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2022-04-16 09:46:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `gen_table`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table`;
CREATE TABLE `gen_table` (
  `table_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_name` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '表名称',
  `table_comment` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '表描述',
  `sub_table_name` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '关联子表的表名',
  `sub_table_fk_name` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '子表关联的外键名',
  `class_name` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '实体类名称',
  `tpl_category` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT 'crud' COMMENT '使用的模板（crud单表操作 tree树表操作）',
  `package_name` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '生成包路径',
  `module_name` varchar(30) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '生成模块名',
  `business_name` varchar(30) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '生成业务名',
  `function_name` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '生成功能名',
  `function_author` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '生成功能作者',
  `gen_type` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '生成代码方式（0zip压缩包 1自定义路径）',
  `gen_path` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT '/' COMMENT '生成路径（不填默认项目路径）',
  `options` varchar(1000) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '其它生成选项',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`table_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='代码生成业务表';

-- ----------------------------
-- Records of gen_table
-- ----------------------------
INSERT INTO `gen_table` VALUES ('1', 't_waste_disposal_info', '危险废物处置信息表', null, null, 'WasteDisposalInfo', 'crud', 'com.ruoyi.system', 'wasteDisposal', 'info', '危险废物处置信息', 'wind', '0', '/', '{}', 'admin', '2022-03-19 18:43:09', '', '2022-03-19 18:46:31', null);
INSERT INTO `gen_table` VALUES ('2', 't_handle_step_config', '处理步骤配置表', null, null, 'HandleStepConfig', 'crud', 'com.ruoyi.system', 'handleStep', 'config', '处理步骤配置', 'wind', '0', '/', '{}', 'admin', '2022-03-21 14:29:32', '', '2022-03-21 14:30:49', null);
INSERT INTO `gen_table` VALUES ('3', 't_handle_log', '处理记录表', null, null, 'HandleLog', 'crud', 'com.ruoyi.system', 'handle', 'log', '处理记录', 'wind', '0', '/', '{}', 'admin', '2022-03-22 15:22:55', '', '2022-03-22 15:23:58', null);

-- ----------------------------
-- Table structure for `gen_table_column`
-- ----------------------------
DROP TABLE IF EXISTS `gen_table_column`;
CREATE TABLE `gen_table_column` (
  `column_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '编号',
  `table_id` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '归属表编号',
  `column_name` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '列名称',
  `column_comment` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '列描述',
  `column_type` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '列类型',
  `java_type` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'JAVA类型',
  `java_field` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT 'JAVA字段名',
  `is_pk` char(1) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '是否主键（1是）',
  `is_increment` char(1) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '是否自增（1是）',
  `is_required` char(1) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '是否必填（1是）',
  `is_insert` char(1) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '是否为插入字段（1是）',
  `is_edit` char(1) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '是否编辑字段（1是）',
  `is_list` char(1) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '是否列表字段（1是）',
  `is_query` char(1) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '是否查询字段（1是）',
  `query_type` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT 'EQ' COMMENT '查询方式（等于、不等于、大于、小于、范围）',
  `html_type` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '显示类型（文本框、文本域、下拉框、复选框、单选框、日期控件）',
  `dict_type` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`column_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='代码生成业务表字段';

-- ----------------------------
-- Records of gen_table_column
-- ----------------------------
INSERT INTO `gen_table_column` VALUES ('1', '1', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '1', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-03-19 18:43:09', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('2', '1', 'name', '名称', 'varchar(255)', 'String', 'name', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '2', 'admin', '2022-03-19 18:43:09', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('3', '1', 'num', '数量', 'varchar(10)', 'String', 'num', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2022-03-19 18:43:09', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('4', '1', 'unit', '单位', 'varchar(2)', 'String', 'unit', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2022-03-19 18:43:09', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('5', '1', 'plan', '是否计划', 'varchar(2)', 'String', 'plan', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '5', 'admin', '2022-03-19 18:43:09', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('6', '1', 'state', '形态', 'varchar(2)', 'String', 'state', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '6', 'admin', '2022-03-19 18:43:09', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('7', '1', 'monthly', '月份', 'varchar(10)', 'String', 'monthly', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '7', 'admin', '2022-03-19 18:43:09', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('8', '1', 'complete', '是否完成处理', 'varchar(2)', 'String', 'complete', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '8', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('9', '1', 'report', '上报状态', 'varchar(2)', 'String', 'report', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '9', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('10', '1', 'create_dept_id', '创建部门id', 'bigint(20)', 'Long', 'createDeptId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '10', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:31');
INSERT INTO `gen_table_column` VALUES ('11', '1', 'create_dept_name', '创建部门名称', 'varchar(30)', 'String', 'createDeptName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '11', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('12', '1', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, '1', '1', 'EQ', 'input', '', '12', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('13', '1', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '13', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('14', '1', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, 'EQ', 'input', '', '14', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('15', '1', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, 'EQ', 'datetime', '', '15', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('16', '1', 'create_user_id', '创建人id', 'binary(20)', 'String', 'createUserId', '0', '0', null, '1', '1', '1', '1', 'EQ', null, '', '16', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('17', '1', 'create_user_phone', '创建人电话', 'varchar(20)', 'String', 'createUserPhone', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '17', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('18', '1', 'handle_step', '当前处理步骤', 'int(10)', 'Integer', 'handleStep', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '18', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('19', '1', 'handle_dept_id', '当前处理部门id', 'bigint(20)', 'Long', 'handleDeptId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '19', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('20', '1', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', null, '1', null, null, null, 'EQ', 'input', '', '20', 'admin', '2022-03-19 18:43:10', '', '2022-03-19 18:46:32');
INSERT INTO `gen_table_column` VALUES ('21', '2', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '0', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-03-21 14:29:32', '', '2022-03-21 14:30:49');
INSERT INTO `gen_table_column` VALUES ('22', '2', 'state', '形态', 'varchar(2)', 'String', 'state', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2022-03-21 14:29:32', '', '2022-03-21 14:30:49');
INSERT INTO `gen_table_column` VALUES ('23', '2', 'config', '配置', 'longtext', 'String', 'config', '0', '0', null, '1', '1', '1', '1', 'EQ', 'textarea', '', '3', 'admin', '2022-03-21 14:29:32', '', '2022-03-21 14:30:49');
INSERT INTO `gen_table_column` VALUES ('24', '2', 'create_dept_id', '创建部门id', 'bigint(20)', 'Long', 'createDeptId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2022-03-21 14:29:32', '', '2022-03-21 14:30:50');
INSERT INTO `gen_table_column` VALUES ('25', '2', 'create_dept_name', '创建部门名称', 'varchar(30)', 'String', 'createDeptName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '5', 'admin', '2022-03-21 14:29:32', '', '2022-03-21 14:30:50');
INSERT INTO `gen_table_column` VALUES ('26', '2', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, null, null, 'EQ', 'input', '', '6', 'admin', '2022-03-21 14:29:33', '', '2022-03-21 14:30:50');
INSERT INTO `gen_table_column` VALUES ('27', '2', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '7', 'admin', '2022-03-21 14:29:33', '', '2022-03-21 14:30:50');
INSERT INTO `gen_table_column` VALUES ('28', '2', 'update_by', '更新者', 'varchar(64)', 'String', 'updateBy', '0', '0', null, '1', '1', null, null, 'EQ', 'input', '', '8', 'admin', '2022-03-21 14:29:33', '', '2022-03-21 14:30:50');
INSERT INTO `gen_table_column` VALUES ('29', '2', 'update_time', '更新时间', 'datetime', 'Date', 'updateTime', '0', '0', null, '1', '1', null, null, 'EQ', 'datetime', '', '9', 'admin', '2022-03-21 14:29:33', '', '2022-03-21 14:30:50');
INSERT INTO `gen_table_column` VALUES ('30', '2', 'create_user_phone', '创建人电话', 'varchar(20)', 'String', 'createUserPhone', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '10', 'admin', '2022-03-21 14:29:33', '', '2022-03-21 14:30:50');
INSERT INTO `gen_table_column` VALUES ('31', '2', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', null, '1', null, null, null, 'EQ', 'input', '', '11', 'admin', '2022-03-21 14:29:33', '', '2022-03-21 14:30:50');
INSERT INTO `gen_table_column` VALUES ('32', '3', 'id', '主键', 'bigint(20)', 'Long', 'id', '1', '0', null, '1', null, null, null, 'EQ', 'input', '', '1', 'admin', '2022-03-22 15:22:55', '', '2022-03-22 15:23:58');
INSERT INTO `gen_table_column` VALUES ('33', '3', 'waste_id', '废物处理id', 'bigint(20)', 'Long', 'wasteId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '2', 'admin', '2022-03-22 15:22:55', '', '2022-03-22 15:23:58');
INSERT INTO `gen_table_column` VALUES ('34', '3', 'step', '处理步骤', 'int(10)', 'Integer', 'step', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '3', 'admin', '2022-03-22 15:22:55', '', '2022-03-22 15:23:58');
INSERT INTO `gen_table_column` VALUES ('35', '3', 'create_dept_id', '创建部门id', 'bigint(20)', 'Long', 'createDeptId', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '4', 'admin', '2022-03-22 15:22:55', '', '2022-03-22 15:23:58');
INSERT INTO `gen_table_column` VALUES ('36', '3', 'create_dept_name', '创建部门名称', 'varchar(30)', 'String', 'createDeptName', '0', '0', null, '1', '1', '1', '1', 'LIKE', 'input', '', '5', 'admin', '2022-03-22 15:22:55', '', '2022-03-22 15:23:58');
INSERT INTO `gen_table_column` VALUES ('37', '3', 'create_by', '创建者', 'varchar(64)', 'String', 'createBy', '0', '0', null, '1', null, null, null, 'EQ', 'input', '', '6', 'admin', '2022-03-22 15:22:55', '', '2022-03-22 15:23:58');
INSERT INTO `gen_table_column` VALUES ('38', '3', 'create_time', '创建时间', 'datetime', 'Date', 'createTime', '0', '0', null, '1', null, null, null, 'EQ', 'datetime', '', '7', 'admin', '2022-03-22 15:22:56', '', '2022-03-22 15:23:58');
INSERT INTO `gen_table_column` VALUES ('39', '3', 'create_user_phone', '创建人电话', 'varchar(20)', 'String', 'createUserPhone', '0', '0', null, '1', '1', '1', '1', 'EQ', 'input', '', '8', 'admin', '2022-03-22 15:22:56', '', '2022-03-22 15:23:58');
INSERT INTO `gen_table_column` VALUES ('40', '3', 'del_flag', '删除标志（0代表存在 2代表删除）', 'char(1)', 'String', 'delFlag', '0', '0', null, '1', null, null, null, 'EQ', 'input', '', '9', 'admin', '2022-03-22 15:22:56', '', '2022-03-22 15:23:58');

-- ----------------------------
-- Table structure for `sys_config`
-- ----------------------------
DROP TABLE IF EXISTS `sys_config`;
CREATE TABLE `sys_config` (
  `config_id` int(5) NOT NULL AUTO_INCREMENT COMMENT '参数主键',
  `config_name` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '参数名称',
  `config_key` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '参数键名',
  `config_value` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '参数键值',
  `config_type` char(1) COLLATE utf8mb4_croatian_ci DEFAULT 'N' COMMENT '系统内置（Y是 N否）',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`config_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='参数配置表';

-- ----------------------------
-- Records of sys_config
-- ----------------------------
INSERT INTO `sys_config` VALUES ('1', '主框架页-默认皮肤样式名称', 'sys.index.skinName', 'skin-blue', 'Y', 'admin', '2022-03-18 13:59:45', '', null, '蓝色 skin-blue、绿色 skin-green、紫色 skin-purple、红色 skin-red、黄色 skin-yellow');
INSERT INTO `sys_config` VALUES ('2', '用户管理-账号初始密码', 'sys.user.initPassword', '123456', 'Y', 'admin', '2022-03-18 13:59:45', '', null, '初始化密码 123456');
INSERT INTO `sys_config` VALUES ('3', '主框架页-侧边栏主题', 'sys.index.sideTheme', 'theme-dark', 'Y', 'admin', '2022-03-18 13:59:45', '', null, '深色主题theme-dark，浅色主题theme-light');
INSERT INTO `sys_config` VALUES ('4', '账号自助-验证码开关', 'sys.account.captchaOnOff', 'true', 'Y', 'admin', '2022-03-18 13:59:45', '', null, '是否开启验证码功能（true开启，false关闭）');
INSERT INTO `sys_config` VALUES ('5', '账号自助-是否开启用户注册功能', 'sys.account.registerUser', 'false', 'Y', 'admin', '2022-03-18 13:59:45', '', null, '是否开启注册用户功能（true开启，false关闭）');

-- ----------------------------
-- Table structure for `sys_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dept`;
CREATE TABLE `sys_dept` (
  `dept_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '部门id',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父部门id',
  `ancestors` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '祖级列表',
  `dept_name` varchar(30) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '部门名称',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `leader` varchar(20) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '负责人',
  `phone` varchar(11) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '联系电话',
  `area_code` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '区域编码',
  `representative` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '法人代表',
  `address` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '地址',
  `organization` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '机构代码',
  `email` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '邮箱',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '部门状态（0正常 1停用）',
  `del_flag` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`dept_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='部门表';

-- ----------------------------
-- Records of sys_dept
-- ----------------------------
INSERT INTO `sys_dept` VALUES ('100', '0', '0', '环保局', '0', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '0', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-23 18:18:47');
INSERT INTO `sys_dept` VALUES ('101', '100', '0,100', '石嘴山环保局', '1', '若依', '15888888888', '640205000000', '局长', '宁夏是石嘴山', '000000001', 'ry@qq.com', '0', '0', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-24 17:23:20');
INSERT INTO `sys_dept` VALUES ('102', '100', '0,100', '处置中心', '2', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '0', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-23 18:20:52');
INSERT INTO `sys_dept` VALUES ('103', '101', '0,100,101', '研发部门', '1', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '0', 'admin', '2022-03-18 13:59:45', '', null);
INSERT INTO `sys_dept` VALUES ('104', '101', '0,100,101', '市场部门', '2', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '2', 'admin', '2022-03-18 13:59:45', '', null);
INSERT INTO `sys_dept` VALUES ('105', '101', '0,100,101', '测试部门', '3', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '2', 'admin', '2022-03-18 13:59:45', '', null);
INSERT INTO `sys_dept` VALUES ('106', '101', '0,100,101', '财务部门', '4', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '2', 'admin', '2022-03-18 13:59:45', '', null);
INSERT INTO `sys_dept` VALUES ('107', '101', '0,100,101', '运维部门', '5', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '2', 'admin', '2022-03-18 13:59:45', '', null);
INSERT INTO `sys_dept` VALUES ('108', '102', '0,100,102', '焚烧部门', '1', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '0', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-23 18:21:06');
INSERT INTO `sys_dept` VALUES ('109', '102', '0,100,102', '掩埋部门', '2', '若依', '15888888888', null, null, null, null, 'ry@qq.com', '0', '0', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-23 18:21:14');
INSERT INTO `sys_dept` VALUES ('200', '100', '0,100', '石嘴山XX洗煤厂', '3', '张三', '18888888888', null, null, null, null, null, '0', '0', 'admin', '2022-03-23 18:21:59', '', null);

-- ----------------------------
-- Table structure for `sys_dict_data`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_data`;
CREATE TABLE `sys_dict_data` (
  `dict_code` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典编码',
  `dict_sort` int(4) DEFAULT '0' COMMENT '字典排序',
  `dict_label` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '字典标签',
  `dict_value` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '字典键值',
  `dict_type` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '字典类型',
  `css_class` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '样式属性（其他样式扩展）',
  `list_class` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '表格回显样式',
  `is_default` char(1) COLLATE utf8mb4_croatian_ci DEFAULT 'N' COMMENT '是否默认（Y是 N否）',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_code`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='字典数据表';

-- ----------------------------
-- Records of sys_dict_data
-- ----------------------------
INSERT INTO `sys_dict_data` VALUES ('1', '1', '男', '0', 'sys_user_sex', '', '', 'Y', '0', 'admin', '2022-03-18 13:59:45', '', null, '性别男');
INSERT INTO `sys_dict_data` VALUES ('2', '2', '女', '1', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '性别女');
INSERT INTO `sys_dict_data` VALUES ('3', '3', '未知', '2', 'sys_user_sex', '', '', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '性别未知');
INSERT INTO `sys_dict_data` VALUES ('4', '1', '显示', '0', 'sys_show_hide', '', 'primary', 'Y', '0', 'admin', '2022-03-18 13:59:45', '', null, '显示菜单');
INSERT INTO `sys_dict_data` VALUES ('5', '2', '隐藏', '1', 'sys_show_hide', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '隐藏菜单');
INSERT INTO `sys_dict_data` VALUES ('6', '1', '正常', '0', 'sys_normal_disable', '', 'primary', 'Y', '0', 'admin', '2022-03-18 13:59:45', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('7', '2', '停用', '1', 'sys_normal_disable', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('8', '1', '正常', '0', 'sys_job_status', '', 'primary', 'Y', '0', 'admin', '2022-03-18 13:59:45', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('9', '2', '暂停', '1', 'sys_job_status', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('10', '1', '默认', 'DEFAULT', 'sys_job_group', '', '', 'Y', '0', 'admin', '2022-03-18 13:59:45', '', null, '默认分组');
INSERT INTO `sys_dict_data` VALUES ('11', '2', '系统', 'SYSTEM', 'sys_job_group', '', '', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '系统分组');
INSERT INTO `sys_dict_data` VALUES ('12', '1', '是', 'Y', 'sys_yes_no', '', 'primary', 'Y', '0', 'admin', '2022-03-18 13:59:45', '', null, '系统默认是');
INSERT INTO `sys_dict_data` VALUES ('13', '2', '否', 'N', 'sys_yes_no', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '系统默认否');
INSERT INTO `sys_dict_data` VALUES ('14', '1', '通过', '1', 'sys_notice_type', '', 'success', 'Y', '0', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-23 16:55:36', '通知');
INSERT INTO `sys_dict_data` VALUES ('15', '2', '警告', '2', 'sys_notice_type', '', 'warning', 'N', '0', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-23 16:55:47', '公告');
INSERT INTO `sys_dict_data` VALUES ('16', '1', '正常', '0', 'sys_notice_status', '', 'primary', 'Y', '0', 'admin', '2022-03-18 13:59:45', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('17', '2', '关闭', '1', 'sys_notice_status', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '关闭状态');
INSERT INTO `sys_dict_data` VALUES ('18', '1', '新增', '1', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '新增操作');
INSERT INTO `sys_dict_data` VALUES ('19', '2', '修改', '2', 'sys_oper_type', '', 'info', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '修改操作');
INSERT INTO `sys_dict_data` VALUES ('20', '3', '删除', '3', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '删除操作');
INSERT INTO `sys_dict_data` VALUES ('21', '4', '授权', '4', 'sys_oper_type', '', 'primary', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '授权操作');
INSERT INTO `sys_dict_data` VALUES ('22', '5', '导出', '5', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '导出操作');
INSERT INTO `sys_dict_data` VALUES ('23', '6', '导入', '6', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '导入操作');
INSERT INTO `sys_dict_data` VALUES ('24', '7', '强退', '7', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '强退操作');
INSERT INTO `sys_dict_data` VALUES ('25', '8', '生成代码', '8', 'sys_oper_type', '', 'warning', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '生成操作');
INSERT INTO `sys_dict_data` VALUES ('26', '9', '清空数据', '9', 'sys_oper_type', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '清空操作');
INSERT INTO `sys_dict_data` VALUES ('27', '1', '成功', '0', 'sys_common_status', '', 'primary', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '正常状态');
INSERT INTO `sys_dict_data` VALUES ('28', '2', '失败', '1', 'sys_common_status', '', 'danger', 'N', '0', 'admin', '2022-03-18 13:59:45', '', null, '停用状态');
INSERT INTO `sys_dict_data` VALUES ('100', '0', '固态', '1', 'dict_wastes_state', null, 'default', 'N', '0', 'admin', '2022-03-21 20:21:49', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('101', '1', '液态', '2', 'dict_wastes_state', null, 'default', 'N', '0', 'admin', '2022-03-21 20:21:57', 'admin', '2022-03-21 20:22:05', null);
INSERT INTO `sys_dict_data` VALUES ('102', '2', '气态', '3', 'dict_wastes_state', null, 'default', 'N', '0', 'admin', '2022-03-21 20:22:13', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('103', '0', '克', 'g', 'dict_unit_type', null, 'default', 'N', '0', 'admin', '2022-03-23 09:45:04', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('104', '1', '千克', 'kg', 'dict_unit_type', null, 'default', 'N', '0', 'admin', '2022-03-23 09:45:22', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('105', '2', '吨', 'k', 'dict_unit_type', null, 'default', 'N', '0', 'admin', '2022-03-23 09:45:39', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('106', '0', '大武口区', '640202000000', 'dict_area_code', null, 'default', 'N', '0', 'admin', '2022-03-24 17:20:08', '', null, null);
INSERT INTO `sys_dict_data` VALUES ('107', '1', '惠农区', '640205000000', 'dict_area_code', null, 'default', 'N', '0', 'admin', '2022-03-24 17:20:28', '', null, null);

-- ----------------------------
-- Table structure for `sys_dict_type`
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict_type`;
CREATE TABLE `sys_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典主键',
  `dict_name` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '字典名称',
  `dict_type` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '字典类型',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`dict_id`) USING BTREE,
  UNIQUE KEY `dict_type` (`dict_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='字典类型表';

-- ----------------------------
-- Records of sys_dict_type
-- ----------------------------
INSERT INTO `sys_dict_type` VALUES ('1', '用户性别', 'sys_user_sex', '0', 'admin', '2022-03-18 13:59:45', '', null, '用户性别列表');
INSERT INTO `sys_dict_type` VALUES ('2', '菜单状态', 'sys_show_hide', '0', 'admin', '2022-03-18 13:59:45', '', null, '菜单状态列表');
INSERT INTO `sys_dict_type` VALUES ('3', '系统开关', 'sys_normal_disable', '0', 'admin', '2022-03-18 13:59:45', '', null, '系统开关列表');
INSERT INTO `sys_dict_type` VALUES ('4', '任务状态', 'sys_job_status', '0', 'admin', '2022-03-18 13:59:45', '', null, '任务状态列表');
INSERT INTO `sys_dict_type` VALUES ('5', '任务分组', 'sys_job_group', '0', 'admin', '2022-03-18 13:59:45', '', null, '任务分组列表');
INSERT INTO `sys_dict_type` VALUES ('6', '系统是否', 'sys_yes_no', '0', 'admin', '2022-03-18 13:59:45', '', null, '系统是否列表');
INSERT INTO `sys_dict_type` VALUES ('7', '通知类型', 'sys_notice_type', '0', 'admin', '2022-03-18 13:59:45', '', null, '通知类型列表');
INSERT INTO `sys_dict_type` VALUES ('8', '通知状态', 'sys_notice_status', '0', 'admin', '2022-03-18 13:59:45', '', null, '通知状态列表');
INSERT INTO `sys_dict_type` VALUES ('9', '操作类型', 'sys_oper_type', '0', 'admin', '2022-03-18 13:59:45', '', null, '操作类型列表');
INSERT INTO `sys_dict_type` VALUES ('10', '系统状态', 'sys_common_status', '0', 'admin', '2022-03-18 13:59:45', '', null, '登录状态列表');
INSERT INTO `sys_dict_type` VALUES ('100', '危险物品形态', 'dict_wastes_state', '0', 'admin', '2022-03-21 20:21:10', '', null, null);
INSERT INTO `sys_dict_type` VALUES ('101', '常用单位', 'dict_unit_type', '0', 'admin', '2022-03-23 09:44:40', 'admin', '2022-03-23 09:46:22', null);
INSERT INTO `sys_dict_type` VALUES ('102', '所属区域', 'dict_area_code', '0', 'admin', '2022-03-24 17:18:26', '', null, '所属区域');

-- ----------------------------
-- Table structure for `sys_job`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job`;
CREATE TABLE `sys_job` (
  `job_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务ID',
  `job_name` varchar(64) COLLATE utf8mb4_croatian_ci NOT NULL DEFAULT '' COMMENT '任务名称',
  `job_group` varchar(64) COLLATE utf8mb4_croatian_ci NOT NULL DEFAULT 'DEFAULT' COMMENT '任务组名',
  `invoke_target` varchar(500) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '调用目标字符串',
  `cron_expression` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT 'cron执行表达式',
  `misfire_policy` varchar(20) COLLATE utf8mb4_croatian_ci DEFAULT '3' COMMENT '计划执行错误策略（1立即执行 2执行一次 3放弃执行）',
  `concurrent` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '1' COMMENT '是否并发执行（0允许 1禁止）',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '状态（0正常 1暂停）',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '备注信息',
  PRIMARY KEY (`job_id`,`job_name`,`job_group`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='定时任务调度表';

-- ----------------------------
-- Records of sys_job
-- ----------------------------
INSERT INTO `sys_job` VALUES ('1', '系统默认（无参）', 'DEFAULT', 'ryTask.ryNoParams', '0/10 * * * * ?', '3', '1', '1', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_job` VALUES ('2', '系统默认（有参）', 'DEFAULT', 'ryTask.ryParams(\'ry\')', '0/15 * * * * ?', '3', '1', '1', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_job` VALUES ('3', '系统默认（多参）', 'DEFAULT', 'ryTask.ryMultipleParams(\'ry\', true, 2000L, 316.50D, 100)', '0/20 * * * * ?', '3', '1', '1', 'admin', '2022-03-18 13:59:45', '', null, '');

-- ----------------------------
-- Table structure for `sys_job_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_job_log`;
CREATE TABLE `sys_job_log` (
  `job_log_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '任务日志ID',
  `job_name` varchar(64) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '任务名称',
  `job_group` varchar(64) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '任务组名',
  `invoke_target` varchar(500) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '调用目标字符串',
  `job_message` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '日志信息',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '执行状态（0正常 1失败）',
  `exception_info` varchar(2000) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '异常信息',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`job_log_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='定时任务调度日志表';

-- ----------------------------
-- Records of sys_job_log
-- ----------------------------

-- ----------------------------
-- Table structure for `sys_logininfor`
-- ----------------------------
DROP TABLE IF EXISTS `sys_logininfor`;
CREATE TABLE `sys_logininfor` (
  `info_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '访问ID',
  `user_name` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '用户账号',
  `ipaddr` varchar(128) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '登录IP地址',
  `login_location` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '登录地点',
  `browser` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '浏览器类型',
  `os` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '操作系统',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '登录状态（0成功 1失败）',
  `msg` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '提示消息',
  `login_time` datetime DEFAULT NULL COMMENT '访问时间',
  PRIMARY KEY (`info_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='系统访问记录';

-- ----------------------------
-- Records of sys_logininfor
-- ----------------------------
INSERT INTO `sys_logininfor` VALUES ('100', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 14:17:52');
INSERT INTO `sys_logininfor` VALUES ('101', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-18 14:52:02');
INSERT INTO `sys_logininfor` VALUES ('102', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-19 17:59:55');
INSERT INTO `sys_logininfor` VALUES ('103', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-19 17:59:56');
INSERT INTO `sys_logininfor` VALUES ('104', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-19 18:00:02');
INSERT INTO `sys_logininfor` VALUES ('105', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-19 18:00:07');
INSERT INTO `sys_logininfor` VALUES ('106', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 18:00:13');
INSERT INTO `sys_logininfor` VALUES ('107', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 18:42:18');
INSERT INTO `sys_logininfor` VALUES ('108', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-19 18:56:47');
INSERT INTO `sys_logininfor` VALUES ('109', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-19 18:56:52');
INSERT INTO `sys_logininfor` VALUES ('110', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-19 18:56:55');
INSERT INTO `sys_logininfor` VALUES ('111', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-21 09:31:28');
INSERT INTO `sys_logininfor` VALUES ('112', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 09:31:31');
INSERT INTO `sys_logininfor` VALUES ('113', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 10:41:33');
INSERT INTO `sys_logininfor` VALUES ('114', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 13:55:01');
INSERT INTO `sys_logininfor` VALUES ('115', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 14:28:49');
INSERT INTO `sys_logininfor` VALUES ('116', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-21 14:43:12');
INSERT INTO `sys_logininfor` VALUES ('117', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 14:43:16');
INSERT INTO `sys_logininfor` VALUES ('118', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 16:08:42');
INSERT INTO `sys_logininfor` VALUES ('119', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-21 17:30:47');
INSERT INTO `sys_logininfor` VALUES ('120', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 17:30:57');
INSERT INTO `sys_logininfor` VALUES ('121', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-21 19:45:21');
INSERT INTO `sys_logininfor` VALUES ('122', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 14:09:20');
INSERT INTO `sys_logininfor` VALUES ('123', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-22 14:59:22');
INSERT INTO `sys_logininfor` VALUES ('124', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 14:59:25');
INSERT INTO `sys_logininfor` VALUES ('125', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-22 15:34:10');
INSERT INTO `sys_logininfor` VALUES ('126', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 15:34:13');
INSERT INTO `sys_logininfor` VALUES ('127', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 16:36:21');
INSERT INTO `sys_logininfor` VALUES ('128', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 20:20:03');
INSERT INTO `sys_logininfor` VALUES ('129', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 21:05:30');
INSERT INTO `sys_logininfor` VALUES ('130', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-22 21:07:52');
INSERT INTO `sys_logininfor` VALUES ('131', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-22 21:07:56');
INSERT INTO `sys_logininfor` VALUES ('132', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-22 21:08:01');
INSERT INTO `sys_logininfor` VALUES ('133', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 08:53:00');
INSERT INTO `sys_logininfor` VALUES ('134', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 10:00:39');
INSERT INTO `sys_logininfor` VALUES ('135', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 10:00:45');
INSERT INTO `sys_logininfor` VALUES ('136', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 10:17:01');
INSERT INTO `sys_logininfor` VALUES ('137', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 10:17:08');
INSERT INTO `sys_logininfor` VALUES ('138', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 10:57:04');
INSERT INTO `sys_logininfor` VALUES ('139', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-23 10:57:09');
INSERT INTO `sys_logininfor` VALUES ('140', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 10:57:11');
INSERT INTO `sys_logininfor` VALUES ('141', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 14:45:46');
INSERT INTO `sys_logininfor` VALUES ('142', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 16:49:30');
INSERT INTO `sys_logininfor` VALUES ('143', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 17:46:51');
INSERT INTO `sys_logininfor` VALUES ('144', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 17:47:27');
INSERT INTO `sys_logininfor` VALUES ('145', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 17:49:03');
INSERT INTO `sys_logininfor` VALUES ('146', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 17:49:07');
INSERT INTO `sys_logininfor` VALUES ('147', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:31:45');
INSERT INTO `sys_logininfor` VALUES ('148', 'fenshao', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:31:52');
INSERT INTO `sys_logininfor` VALUES ('149', 'fenshao', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:32:42');
INSERT INTO `sys_logininfor` VALUES ('150', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:32:59');
INSERT INTO `sys_logininfor` VALUES ('151', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:33:14');
INSERT INTO `sys_logininfor` VALUES ('152', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-23 18:33:22');
INSERT INTO `sys_logininfor` VALUES ('153', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:33:29');
INSERT INTO `sys_logininfor` VALUES ('154', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:35:01');
INSERT INTO `sys_logininfor` VALUES ('155', 'fenshao', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:35:25');
INSERT INTO `sys_logininfor` VALUES ('156', 'fenshao', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:36:51');
INSERT INTO `sys_logininfor` VALUES ('157', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:37:03');
INSERT INTO `sys_logininfor` VALUES ('158', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:37:24');
INSERT INTO `sys_logininfor` VALUES ('159', 'fenshao', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:37:33');
INSERT INTO `sys_logininfor` VALUES ('160', 'fenshao', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:38:01');
INSERT INTO `sys_logininfor` VALUES ('161', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:38:09');
INSERT INTO `sys_logininfor` VALUES ('162', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:38:29');
INSERT INTO `sys_logininfor` VALUES ('163', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-23 18:38:40');
INSERT INTO `sys_logininfor` VALUES ('164', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:38:44');
INSERT INTO `sys_logininfor` VALUES ('165', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:39:28');
INSERT INTO `sys_logininfor` VALUES ('166', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:39:39');
INSERT INTO `sys_logininfor` VALUES ('167', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-23 18:40:21');
INSERT INTO `sys_logininfor` VALUES ('168', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-23 18:40:29');
INSERT INTO `sys_logininfor` VALUES ('169', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-23 18:40:32');
INSERT INTO `sys_logininfor` VALUES ('170', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 08:57:26');
INSERT INTO `sys_logininfor` VALUES ('171', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 09:01:13');
INSERT INTO `sys_logininfor` VALUES ('172', 'fenshao', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:01:20');
INSERT INTO `sys_logininfor` VALUES ('173', 'fenshao', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 09:01:35');
INSERT INTO `sys_logininfor` VALUES ('174', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:01:47');
INSERT INTO `sys_logininfor` VALUES ('175', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 09:04:14');
INSERT INTO `sys_logininfor` VALUES ('176', 'huabaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-24 09:04:24');
INSERT INTO `sys_logininfor` VALUES ('177', 'huabaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '登录用户：huabaoju 不存在', '2022-03-24 09:04:27');
INSERT INTO `sys_logininfor` VALUES ('178', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:04:35');
INSERT INTO `sys_logininfor` VALUES ('179', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 09:08:01');
INSERT INTO `sys_logininfor` VALUES ('180', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:08:09');
INSERT INTO `sys_logininfor` VALUES ('181', 'qiye', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 09:50:08');
INSERT INTO `sys_logininfor` VALUES ('182', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:50:11');
INSERT INTO `sys_logininfor` VALUES ('183', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 09:52:24');
INSERT INTO `sys_logininfor` VALUES ('184', '掩埋', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '登录用户：掩埋 不存在', '2022-03-24 09:52:35');
INSERT INTO `sys_logininfor` VALUES ('185', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-24 09:52:39');
INSERT INTO `sys_logininfor` VALUES ('186', 'yanmai', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 09:52:42');
INSERT INTO `sys_logininfor` VALUES ('187', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码错误', '2022-03-24 16:22:14');
INSERT INTO `sys_logininfor` VALUES ('188', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 16:22:18');
INSERT INTO `sys_logininfor` VALUES ('189', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 17:16:10');
INSERT INTO `sys_logininfor` VALUES ('190', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 17:48:17');
INSERT INTO `sys_logininfor` VALUES ('191', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 17:48:21');
INSERT INTO `sys_logininfor` VALUES ('192', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 17:56:48');
INSERT INTO `sys_logininfor` VALUES ('193', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 17:57:23');
INSERT INTO `sys_logininfor` VALUES ('194', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '验证码已失效', '2022-03-24 18:03:14');
INSERT INTO `sys_logininfor` VALUES ('195', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 18:03:18');
INSERT INTO `sys_logininfor` VALUES ('196', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 18:24:52');
INSERT INTO `sys_logininfor` VALUES ('197', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '1', '用户不存在/密码错误', '2022-03-24 18:25:01');
INSERT INTO `sys_logininfor` VALUES ('198', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 18:25:13');
INSERT INTO `sys_logininfor` VALUES ('199', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 18:26:43');
INSERT INTO `sys_logininfor` VALUES ('200', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 18:27:00');
INSERT INTO `sys_logininfor` VALUES ('201', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 18:42:17');
INSERT INTO `sys_logininfor` VALUES ('202', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-24 18:51:10');
INSERT INTO `sys_logininfor` VALUES ('203', 'huanbaoju', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '退出成功', '2022-03-24 18:55:42');
INSERT INTO `sys_logininfor` VALUES ('204', 'admin', '127.0.0.1', '内网IP', 'Chrome 9', 'Windows 10', '0', '登录成功', '2022-03-26 09:39:37');

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_name` varchar(50) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '菜单名称',
  `parent_id` bigint(20) DEFAULT '0' COMMENT '父菜单ID',
  `order_num` int(4) DEFAULT '0' COMMENT '显示顺序',
  `path` varchar(200) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '路由地址',
  `component` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '组件路径',
  `query` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '路由参数',
  `is_frame` int(1) DEFAULT '1' COMMENT '是否为外链（0是 1否）',
  `is_cache` int(1) DEFAULT '0' COMMENT '是否缓存（0缓存 1不缓存）',
  `menu_type` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '菜单类型（M目录 C菜单 F按钮）',
  `visible` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '菜单状态（0显示 1隐藏）',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '菜单状态（0正常 1停用）',
  `perms` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '权限标识',
  `icon` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '#' COMMENT '菜单图标',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '备注',
  PRIMARY KEY (`menu_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2041 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='菜单权限表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('1', '系统管理', '0', '4', 'system', null, '', '1', '0', 'M', '0', '0', '', 'system', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-19 18:53:02', '系统管理目录');
INSERT INTO `sys_menu` VALUES ('2', '系统监控', '0', '5', 'monitor', null, '', '1', '0', 'M', '0', '0', '', 'monitor', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-19 18:53:13', '系统监控目录');
INSERT INTO `sys_menu` VALUES ('3', '系统工具', '0', '6', 'tool', null, '', '1', '0', 'M', '0', '0', '', 'tool', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-19 18:53:19', '系统工具目录');
INSERT INTO `sys_menu` VALUES ('100', '用户管理', '1', '1', 'user', 'system/user/index', '', '1', '0', 'C', '0', '0', 'system:user:list', 'user', 'admin', '2022-03-18 13:59:45', '', null, '用户管理菜单');
INSERT INTO `sys_menu` VALUES ('101', '角色管理', '1', '2', 'role', 'system/role/index', '', '1', '0', 'C', '0', '0', 'system:role:list', 'peoples', 'admin', '2022-03-18 13:59:45', '', null, '角色管理菜单');
INSERT INTO `sys_menu` VALUES ('102', '菜单管理', '1', '3', 'menu', 'system/menu/index', '', '1', '0', 'C', '0', '0', 'system:menu:list', 'tree-table', 'admin', '2022-03-18 13:59:45', '', null, '菜单管理菜单');
INSERT INTO `sys_menu` VALUES ('103', '部门管理', '1', '4', 'dept', 'system/dept/index', '', '1', '0', 'C', '0', '0', 'system:dept:list', 'tree', 'admin', '2022-03-18 13:59:45', '', null, '部门管理菜单');
INSERT INTO `sys_menu` VALUES ('104', '岗位管理', '1', '5', 'post', 'system/post/index', '', '1', '0', 'C', '0', '0', 'system:post:list', 'post', 'admin', '2022-03-18 13:59:45', '', null, '岗位管理菜单');
INSERT INTO `sys_menu` VALUES ('105', '字典管理', '1', '6', 'dict', 'system/dict/index', '', '1', '0', 'C', '0', '0', 'system:dict:list', 'dict', 'admin', '2022-03-18 13:59:45', '', null, '字典管理菜单');
INSERT INTO `sys_menu` VALUES ('106', '参数设置', '1', '7', 'config', 'system/config/index', '', '1', '0', 'C', '0', '0', 'system:config:list', 'edit', 'admin', '2022-03-18 13:59:45', '', null, '参数设置菜单');
INSERT INTO `sys_menu` VALUES ('107', '通知公告', '1', '8', 'notice', 'system/notice/index', '', '1', '0', 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-03-18 13:59:45', '', null, '通知公告菜单');
INSERT INTO `sys_menu` VALUES ('108', '日志管理', '1', '9', 'log', '', '', '1', '0', 'M', '0', '0', '', 'log', 'admin', '2022-03-18 13:59:45', '', null, '日志管理菜单');
INSERT INTO `sys_menu` VALUES ('109', '在线用户', '2', '1', 'online', 'monitor/online/index', '', '1', '0', 'C', '0', '0', 'monitor:online:list', 'online', 'admin', '2022-03-18 13:59:45', '', null, '在线用户菜单');
INSERT INTO `sys_menu` VALUES ('110', '定时任务', '2', '2', 'job', 'monitor/job/index', '', '1', '0', 'C', '0', '0', 'monitor:job:list', 'job', 'admin', '2022-03-18 13:59:45', '', null, '定时任务菜单');
INSERT INTO `sys_menu` VALUES ('111', '数据监控', '2', '3', 'druid', 'monitor/druid/index', '', '1', '0', 'C', '0', '0', 'monitor:druid:list', 'druid', 'admin', '2022-03-18 13:59:45', '', null, '数据监控菜单');
INSERT INTO `sys_menu` VALUES ('112', '服务监控', '2', '4', 'server', 'monitor/server/index', '', '1', '0', 'C', '0', '0', 'monitor:server:list', 'server', 'admin', '2022-03-18 13:59:45', '', null, '服务监控菜单');
INSERT INTO `sys_menu` VALUES ('113', '缓存监控', '2', '5', 'cache', 'monitor/cache/index', '', '1', '0', 'C', '0', '0', 'monitor:cache:list', 'redis', 'admin', '2022-03-18 13:59:45', '', null, '缓存监控菜单');
INSERT INTO `sys_menu` VALUES ('114', '表单构建', '3', '1', 'build', 'tool/build/index', '', '1', '0', 'C', '0', '0', 'tool:build:list', 'build', 'admin', '2022-03-18 13:59:45', '', null, '表单构建菜单');
INSERT INTO `sys_menu` VALUES ('115', '代码生成', '3', '2', 'gen', 'tool/gen/index', '', '1', '0', 'C', '0', '0', 'tool:gen:list', 'code', 'admin', '2022-03-18 13:59:45', '', null, '代码生成菜单');
INSERT INTO `sys_menu` VALUES ('116', '系统接口', '3', '3', 'swagger', 'tool/swagger/index', '', '1', '0', 'C', '0', '0', 'tool:swagger:list', 'swagger', 'admin', '2022-03-18 13:59:45', '', null, '系统接口菜单');
INSERT INTO `sys_menu` VALUES ('500', '操作日志', '108', '1', 'operlog', 'monitor/operlog/index', '', '1', '0', 'C', '0', '0', 'monitor:operlog:list', 'form', 'admin', '2022-03-18 13:59:45', '', null, '操作日志菜单');
INSERT INTO `sys_menu` VALUES ('501', '登录日志', '108', '2', 'logininfor', 'monitor/logininfor/index', '', '1', '0', 'C', '0', '0', 'monitor:logininfor:list', 'logininfor', 'admin', '2022-03-18 13:59:45', '', null, '登录日志菜单');
INSERT INTO `sys_menu` VALUES ('1001', '用户查询', '100', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:user:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1002', '用户新增', '100', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:user:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1003', '用户修改', '100', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:user:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1004', '用户删除', '100', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:user:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1005', '用户导出', '100', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:user:export', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1006', '用户导入', '100', '6', '', '', '', '1', '0', 'F', '0', '0', 'system:user:import', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1007', '重置密码', '100', '7', '', '', '', '1', '0', 'F', '0', '0', 'system:user:resetPwd', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1008', '角色查询', '101', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:role:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1009', '角色新增', '101', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:role:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1010', '角色修改', '101', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:role:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1011', '角色删除', '101', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:role:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1012', '角色导出', '101', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:role:export', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1013', '菜单查询', '102', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1014', '菜单新增', '102', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1015', '菜单修改', '102', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1016', '菜单删除', '102', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:menu:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1017', '部门查询', '103', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1018', '部门新增', '103', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1019', '部门修改', '103', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1020', '部门删除', '103', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:dept:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1021', '岗位查询', '104', '1', '', '', '', '1', '0', 'F', '0', '0', 'system:post:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1022', '岗位新增', '104', '2', '', '', '', '1', '0', 'F', '0', '0', 'system:post:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1023', '岗位修改', '104', '3', '', '', '', '1', '0', 'F', '0', '0', 'system:post:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1024', '岗位删除', '104', '4', '', '', '', '1', '0', 'F', '0', '0', 'system:post:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1025', '岗位导出', '104', '5', '', '', '', '1', '0', 'F', '0', '0', 'system:post:export', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1026', '字典查询', '105', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1027', '字典新增', '105', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1028', '字典修改', '105', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1029', '字典删除', '105', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1030', '字典导出', '105', '5', '#', '', '', '1', '0', 'F', '0', '0', 'system:dict:export', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1031', '参数查询', '106', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1032', '参数新增', '106', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1033', '参数修改', '106', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1034', '参数删除', '106', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1035', '参数导出', '106', '5', '#', '', '', '1', '0', 'F', '0', '0', 'system:config:export', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1036', '公告查询', '107', '1', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1037', '公告新增', '107', '2', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1038', '公告修改', '107', '3', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1039', '公告删除', '107', '4', '#', '', '', '1', '0', 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1040', '操作查询', '500', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1041', '操作删除', '500', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1042', '日志导出', '500', '4', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:operlog:export', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1043', '登录查询', '501', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1044', '登录删除', '501', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1045', '日志导出', '501', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:logininfor:export', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1046', '在线查询', '109', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1047', '批量强退', '109', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:batchLogout', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1048', '单条强退', '109', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:online:forceLogout', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1049', '任务查询', '110', '1', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1050', '任务新增', '110', '2', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:add', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1051', '任务修改', '110', '3', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1052', '任务删除', '110', '4', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1053', '状态修改', '110', '5', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:changeStatus', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1054', '任务导出', '110', '7', '#', '', '', '1', '0', 'F', '0', '0', 'monitor:job:export', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1055', '生成查询', '115', '1', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:query', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1056', '生成修改', '115', '2', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:edit', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1057', '生成删除', '115', '3', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:remove', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1058', '导入代码', '115', '2', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:import', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1059', '预览代码', '115', '4', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:preview', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('1060', '生成代码', '115', '5', '#', '', '', '1', '0', 'F', '0', '0', 'tool:gen:code', '#', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_menu` VALUES ('2000', '危险废物管理', '0', '1', 'wasteDisposal', null, null, '1', '0', 'M', '0', '0', null, 'button', 'admin', '2022-03-19 18:52:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2001', '产废信息管理', '2000', '3', 'wasteDisposal/info', 'wasteDisposal/info/index.vue', null, '1', '0', 'C', '0', '0', 'wasteDisposal:info:list', 'chart', 'admin', '2022-03-19 18:55:39', 'admin', '2022-03-23 17:50:31', '');
INSERT INTO `sys_menu` VALUES ('2002', '处置中心', '0', '2', 'handle', null, null, '1', '0', 'M', '0', '0', null, 'nested', 'admin', '2022-03-21 14:39:57', '', null, '');
INSERT INTO `sys_menu` VALUES ('2003', '处置步骤', '2002', '3', 'handleStep', 'handleStep/config/index', null, '1', '0', 'C', '0', '0', 'handleStep:config:list', 'system', 'admin', '2022-03-21 14:41:38', 'admin', '2022-03-23 17:45:47', '');
INSERT INTO `sys_menu` VALUES ('2005', '处置管理', '2002', '2', 'handleWaste', 'wasteDisposal/info/handle', null, '1', '0', 'C', '0', '0', 'wasteDisposal:info:list', 'cascader', 'admin', '2022-03-22 21:07:37', '', null, '');
INSERT INTO `sys_menu` VALUES ('2006', '计划上报', '2000', '2', 'wasteDisposal/plan', 'wasteDisposal/info/plan', null, '1', '0', 'C', '0', '0', 'wasteDisposal:info:list', 'druid', 'admin', '2022-03-23 10:16:54', 'admin', '2022-03-23 17:50:37', '');
INSERT INTO `sys_menu` VALUES ('2007', '环保局', '0', '3', 'EPA', null, null, '1', '0', 'M', '0', '0', null, 'tool', 'admin', '2022-03-23 10:53:23', '', null, '');
INSERT INTO `sys_menu` VALUES ('2008', '处置中心', '2007', '2', 'handleAll', 'wasteDisposal/info/handleAll', null, '1', '0', 'C', '0', '0', 'wasteDisposal:info:list', 'chart', 'admin', '2022-03-23 10:55:42', 'admin', '2022-03-23 17:48:34', '');
INSERT INTO `sys_menu` VALUES ('2009', '计划比对', '2007', '3', 'handleNotify', 'wasteDisposal/info/handleNotify', null, '1', '0', 'C', '0', '0', 'wasteDisposal:info:list', 'clipboard', 'admin', '2022-03-23 10:56:46', 'admin', '2022-03-23 17:48:40', '');
INSERT INTO `sys_menu` VALUES ('2010', '通知公告', '2000', '1', '/notice/receive', 'system/notice/receive', null, '1', '0', 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-03-23 17:43:53', 'admin', '2022-03-23 17:51:28', '');
INSERT INTO `sys_menu` VALUES ('2011', '通知公告', '2002', '1', '/notice/index', 'system/notice/index', null, '1', '0', 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-03-23 17:45:40', 'admin', '2022-03-23 17:51:43', '');
INSERT INTO `sys_menu` VALUES ('2012', '通知公告', '2007', '1', 'EPA/notice/index', 'system/notice/index', null, '1', '0', 'C', '0', '0', 'system:notice:list', 'message', 'admin', '2022-03-23 17:46:45', 'admin', '2022-03-23 17:51:57', '');
INSERT INTO `sys_menu` VALUES ('2013', '通知查询', '2012', '1', '', null, null, '1', '0', 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-03-23 17:59:36', '', null, '');
INSERT INTO `sys_menu` VALUES ('2014', '通知新增', '2012', '2', '', null, null, '1', '0', 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-03-23 17:59:56', '', null, '');
INSERT INTO `sys_menu` VALUES ('2015', '通知修改', '2012', '3', '', null, null, '1', '0', 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-03-23 18:00:16', '', null, '');
INSERT INTO `sys_menu` VALUES ('2016', '通知删除', '2012', '4', '', null, null, '1', '0', 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-03-23 18:00:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2017', '通知查询', '2011', '1', '', null, null, '1', '0', 'F', '0', '0', 'system:notice:query', '#', 'admin', '2022-03-23 18:01:10', '', null, '');
INSERT INTO `sys_menu` VALUES ('2018', '通知新增', '2011', '2', '', null, null, '1', '0', 'F', '0', '0', 'system:notice:add', '#', 'admin', '2022-03-23 18:01:31', '', null, '');
INSERT INTO `sys_menu` VALUES ('2019', '通知编辑', '2011', '3', '', null, null, '1', '0', 'F', '0', '0', 'system:notice:edit', '#', 'admin', '2022-03-23 18:01:57', '', null, '');
INSERT INTO `sys_menu` VALUES ('2020', '通知删除', '2011', '4', '', null, null, '1', '0', 'F', '0', '0', 'system:notice:remove', '#', 'admin', '2022-03-23 18:02:20', '', null, '');
INSERT INTO `sys_menu` VALUES ('2021', '查询', '2006', '1', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:query', '#', 'admin', '2022-03-23 18:06:02', '', null, '');
INSERT INTO `sys_menu` VALUES ('2022', '新增', '2006', '2', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:add', '#', 'admin', '2022-03-23 18:06:22', '', null, '');
INSERT INTO `sys_menu` VALUES ('2023', '编辑', '2006', '3', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:edit', '#', 'admin', '2022-03-23 18:06:44', '', null, '');
INSERT INTO `sys_menu` VALUES ('2024', '删除', '2006', '4', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:remove', '#', 'admin', '2022-03-23 18:07:03', 'admin', '2022-03-23 18:07:12', '');
INSERT INTO `sys_menu` VALUES ('2025', '上报', '2006', '4', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:report', '#', 'admin', '2022-03-23 18:08:04', '', null, '');
INSERT INTO `sys_menu` VALUES ('2026', '查询', '2001', '1', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:query', '#', 'admin', '2022-03-23 18:08:42', '', null, '');
INSERT INTO `sys_menu` VALUES ('2027', '新增', '2001', '2', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:add', '#', 'admin', '2022-03-23 18:09:12', '', null, '');
INSERT INTO `sys_menu` VALUES ('2028', '编辑', '2001', '3', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:edit', '#', 'admin', '2022-03-23 18:09:32', '', null, '');
INSERT INTO `sys_menu` VALUES ('2029', '删除', '2001', '4', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:remove', '#', 'admin', '2022-03-23 18:09:55', '', null, '');
INSERT INTO `sys_menu` VALUES ('2030', '上报', '2001', '5', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:report', '#', 'admin', '2022-03-23 18:10:13', '', null, '');
INSERT INTO `sys_menu` VALUES ('2031', '查询', '2005', '1', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:query', '#', 'admin', '2022-03-23 18:11:46', '', null, '');
INSERT INTO `sys_menu` VALUES ('2032', '处理', '2005', '2', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:handleWaste', '#', 'admin', '2022-03-23 18:12:16', '', null, '');
INSERT INTO `sys_menu` VALUES ('2033', '处置记录', '2005', '3', '', null, null, '1', '0', 'F', '0', '0', 'handle:log:list', '#', 'admin', '2022-03-23 18:13:18', '', null, '');
INSERT INTO `sys_menu` VALUES ('2034', '查询', '2003', '1', '', null, null, '1', '0', 'F', '0', '0', 'handleStep:config:query', '#', 'admin', '2022-03-23 18:13:47', '', null, '');
INSERT INTO `sys_menu` VALUES ('2035', '新增', '2003', '2', '', null, null, '1', '0', 'F', '0', '0', 'handleStep:config:add', '#', 'admin', '2022-03-23 18:14:07', '', null, '');
INSERT INTO `sys_menu` VALUES ('2036', '修改', '2003', '3', '', null, null, '1', '0', 'F', '0', '0', 'handleStep:config:edit', '#', 'admin', '2022-03-23 18:14:28', '', null, '');
INSERT INTO `sys_menu` VALUES ('2037', '删除', '2003', '4', '', null, null, '1', '0', 'F', '0', '0', 'handleStep:config:remove', '#', 'admin', '2022-03-23 18:14:51', '', null, '');
INSERT INTO `sys_menu` VALUES ('2038', '查询', '2008', '1', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:query', '#', 'admin', '2022-03-23 18:15:55', '', null, '');
INSERT INTO `sys_menu` VALUES ('2039', '处置记录', '2008', '2', '', null, null, '1', '0', 'F', '0', '0', 'handle:log:list', '#', 'admin', '2022-03-23 18:16:16', '', null, '');
INSERT INTO `sys_menu` VALUES ('2040', '计划比对', '2009', '1', '', null, null, '1', '0', 'F', '0', '0', 'wasteDisposal:info:compareList', '#', 'admin', '2022-03-23 18:17:18', '', null, '');

-- ----------------------------
-- Table structure for `sys_notice`
-- ----------------------------
DROP TABLE IF EXISTS `sys_notice`;
CREATE TABLE `sys_notice` (
  `notice_id` int(4) NOT NULL AUTO_INCREMENT COMMENT '公告ID',
  `notice_title` varchar(255) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '公告标题',
  `notice_type` char(1) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '公告类型（1通过 2警告）',
  `notice_content` longblob COMMENT '公告内容',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '公告状态（0正常 1关闭）',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '备注',
  `create_dept_id` bigint(20) DEFAULT NULL COMMENT '创建部门id',
  `create_dept_name` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '创建部门名称',
  `receive_dept_id` bigint(20) DEFAULT NULL COMMENT '接收部门id',
  `receive_dept_name` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '接收部门名称',
  PRIMARY KEY (`notice_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='通知公告表';

-- ----------------------------
-- Records of sys_notice
-- ----------------------------
INSERT INTO `sys_notice` VALUES ('12', '2022-02月度固态废物第1部处理完成', '1', null, '0', 'fenshao', '2022-03-23 18:37:54', '', null, null, '108', '焚烧部门', '200', '石嘴山XX洗煤厂');
INSERT INTO `sys_notice` VALUES ('13', '2022-02月度固态废物处理全部完成', '1', null, '0', 'yanmai', '2022-03-23 18:38:56', '', null, null, '109', '掩埋部门', '200', '石嘴山XX洗煤厂');
INSERT INTO `sys_notice` VALUES ('14', '2022-02月度固态废物处理警告', '2', null, '0', 'huanbaoju', '2022-03-23 18:40:16', '', null, null, '101', '石嘴山环保局', '200', '石嘴山XX洗煤厂');
INSERT INTO `sys_notice` VALUES ('15', '2022-04月度固态废物第1部处理完成', '1', null, '0', 'fenshao', '2022-03-24 09:01:31', '', null, null, '108', '焚烧部门', '200', '石嘴山XX洗煤厂');
INSERT INTO `sys_notice` VALUES ('16', '2022-04月度固态废物处理全部完成', '1', null, '0', 'yanmai', '2022-03-24 09:02:23', '', null, null, '109', '掩埋部门', '200', '石嘴山XX洗煤厂');
INSERT INTO `sys_notice` VALUES ('17', '2022-04月度固态废物第2部处理完成', '1', null, '0', 'yanmai', '2022-03-24 09:02:23', '', null, null, '109', '掩埋部门', '200', '石嘴山XX洗煤厂');
INSERT INTO `sys_notice` VALUES ('18', '2022-04月度固态废物处理通过', '1', null, '0', 'huanbaoju', '2022-03-24 09:07:33', '', null, null, '101', '石嘴山环保局', '200', '石嘴山XX洗煤厂');

-- ----------------------------
-- Table structure for `sys_oper_log`
-- ----------------------------
DROP TABLE IF EXISTS `sys_oper_log`;
CREATE TABLE `sys_oper_log` (
  `oper_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '日志主键',
  `title` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '模块标题',
  `business_type` int(2) DEFAULT '0' COMMENT '业务类型（0其它 1新增 2修改 3删除）',
  `method` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '方法名称',
  `request_method` varchar(10) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '请求方式',
  `operator_type` int(1) DEFAULT '0' COMMENT '操作类别（0其它 1后台用户 2手机端用户）',
  `oper_name` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '操作人员',
  `dept_name` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '部门名称',
  `oper_url` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '请求URL',
  `oper_ip` varchar(128) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '主机地址',
  `oper_location` varchar(255) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '操作地点',
  `oper_param` varchar(2000) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '请求参数',
  `json_result` varchar(2000) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '返回参数',
  `status` int(1) DEFAULT '0' COMMENT '操作状态（0正常 1异常）',
  `error_msg` varchar(2000) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '错误消息',
  `oper_time` datetime DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`oper_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=325 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='操作日志记录';

-- ----------------------------
-- Records of sys_oper_log
-- ----------------------------
INSERT INTO `sys_oper_log` VALUES ('100', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 't_waste_disposal_info', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:43:11');
INSERT INTO `sys_oper_log` VALUES ('101', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"wind\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":1,\"isIncrement\":\"1\",\"increment\":true,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647686589000,\"tableId\":1,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"Name\",\"usableColumn\":false,\"columnId\":2,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"name\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"名称\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"LIKE\",\"columnType\":\"varchar(255)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647686589000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"name\"},{\"capJavaField\":\"Num\",\"usableColumn\":false,\"columnId\":3,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"num\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"数量\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647686589000,\"isEdit\":\"1\",\"tableId\":1,\"pk\":false,\"columnName\":\"num\"},{\"capJavaField\":\"Unit\",\"usableColumn\":false,\"columnId\":4,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"unit\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"单位\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(2)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647686589000,\"isEdit\":\"1\",\"tableId\"', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:46:32');
INSERT INTO `sys_oper_log` VALUES ('102', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', null, '0', null, '2022-03-19 18:46:52');
INSERT INTO `sys_oper_log` VALUES ('103', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', '0', null, '2022-03-19 18:50:46');
INSERT INTO `sys_oper_log` VALUES ('104', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"button\",\"orderNum\":1,\"menuName\":\"危险废物管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"wasteDisposal\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:52:45');
INSERT INTO `sys_oper_log` VALUES ('105', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"system\",\"orderNum\":4,\"menuName\":\"系统管理\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"system\",\"children\":[],\"createTime\":1647583185000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":1,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:53:02');
INSERT INTO `sys_oper_log` VALUES ('106', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"monitor\",\"orderNum\":5,\"menuName\":\"系统监控\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"monitor\",\"children\":[],\"createTime\":1647583185000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:53:13');
INSERT INTO `sys_oper_log` VALUES ('107', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"tool\",\"orderNum\":6,\"menuName\":\"系统工具\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"tool\",\"children\":[],\"createTime\":1647583185000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":3,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:53:19');
INSERT INTO `sys_oper_log` VALUES ('108', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"query\":\"\",\"icon\":\"guide\",\"orderNum\":7,\"menuName\":\"若依官网\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"http://ruoyi.vip\",\"children\":[],\"createTime\":1647583185000,\"updateBy\":\"admin\",\"isFrame\":\"0\",\"menuId\":4,\"menuType\":\"M\",\"perms\":\"\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:53:27');
INSERT INTO `sys_oper_log` VALUES ('109', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":1,\"menuName\":\"上报计划\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/info\",\"component\":\"wasteDisposal/info/index.vue\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:55:39');
INSERT INTO `sys_oper_log` VALUES ('110', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":1,\"menuName\":\"上报计划\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/info\",\"component\":\"wasteDisposal/info/index.vue\",\"children\":[],\"createTime\":1647687339000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:56:10');
INSERT INTO `sys_oper_log` VALUES ('111', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"num\":\"10\",\"params\":{},\"unit\":\"吨\",\"createTime\":1647687573648,\"monthly\":\"2022-01\",\"name\":\"危险品\",\"report\":\"0\",\"id\":1,\"state\":\"固态\",\"complete\":\"0\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 18:59:32');
INSERT INTO `sys_oper_log` VALUES ('112', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"num\":\"10\",\"updateTime\":1647687881550,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"\",\"unit\":\"吨\",\"createTime\":1647687574000,\"updateBy\":\"\",\"monthly\":\"2022-01\",\"name\":\"危险品\",\"report\":\"0\",\"id\":1,\"state\":\"固态\",\"complete\":\"0\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-19 19:04:40');
INSERT INTO `sys_oper_log` VALUES ('113', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 't_handle_step_config', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 14:29:33');
INSERT INTO `sys_oper_log` VALUES ('114', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"wind\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":21,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647844172000,\"tableId\":2,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"State\",\"usableColumn\":false,\"columnId\":22,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"state\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"形态\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"varchar(2)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647844172000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"state\"},{\"capJavaField\":\"Config\",\"usableColumn\":false,\"columnId\":23,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"config\",\"htmlType\":\"textarea\",\"edit\":true,\"query\":true,\"columnComment\":\"配置\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"String\",\"queryType\":\"EQ\",\"columnType\":\"longtext\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647844172000,\"isEdit\":\"1\",\"tableId\":2,\"pk\":false,\"columnName\":\"config\"},{\"capJavaField\":\"CreateDeptId\",\"usableColumn\":false,\"columnId\":24,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createDeptId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建部门id\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":16478', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 14:30:50');
INSERT INTO `sys_oper_log` VALUES ('115', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', null, '0', null, '2022-03-21 14:30:56');
INSERT INTO `sys_oper_log` VALUES ('116', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"nested\",\"orderNum\":2,\"menuName\":\"处置中心\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"handle\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 14:39:57');
INSERT INTO `sys_oper_log` VALUES ('117', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"system\",\"orderNum\":1,\"menuName\":\"处置步骤\",\"params\":{},\"parentId\":2002,\"isCache\":\"0\",\"path\":\"handleStep\",\"component\":\"handleStep/config/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"handleStep:config:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 14:41:38');
INSERT INTO `sys_oper_log` VALUES ('118', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":1,\"params\":{},\"createTime\":1647845377769,\"state\":\"1\",\"config\":\"1\"}', null, '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [E:\\workspace\\RuoYi-Vue\\ruoyi-system\\target\\classes\\mapper\\handleStep\\HandleStepConfigMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.HandleStepConfigMapper.insertHandleStepConfig-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_handle_step_config          ( state,             config,             create_dept_id,                                       create_time )           values ( ?,             ?,             ?,                                       ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-03-21 14:49:31');
INSERT INTO `sys_oper_log` VALUES ('119', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"params\":{},\"createTime\":1647845421697,\"state\":\"1\",\"config\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 14:50:15');
INSERT INTO `sys_oper_log` VALUES ('120', '处理步骤配置', '3', 'com.ruoyi.system.controller.HandleStepConfigController.remove()', 'DELETE', '1', 'admin', null, '/handleStep/1', '127.0.0.1', '内网IP', '{ids=1}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 14:50:19');
INSERT INTO `sys_oper_log` VALUES ('121', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"params\":{},\"createTime\":1647845431849,\"state\":\"1\",\"config\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 14:50:25');
INSERT INTO `sys_oper_log` VALUES ('122', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"updateTime\":1647845440928,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"\",\"createTime\":1647845432000,\"updateBy\":\"\",\"id\":2,\"state\":\"1\",\"config\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 14:50:34');
INSERT INTO `sys_oper_log` VALUES ('123', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":102,\"step\":2}],\"createTime\":1647852999547,\"state\":\"固态\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":102,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 16:56:38');
INSERT INTO `sys_oper_log` VALUES ('124', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":102,\"step\":2},{\"deptId\":109,\"step\":3}],\"createTime\":1647854170722,\"state\":\"对的\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":102,\\\"step\\\":2},{\\\"deptId\\\":109,\\\"step\\\":3}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 17:16:04');
INSERT INTO `sys_oper_log` VALUES ('125', '处理步骤配置', '3', 'com.ruoyi.system.controller.HandleStepConfigController.remove()', 'DELETE', '1', 'admin', null, '/handleStep/2', '127.0.0.1', '内网IP', '{ids=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 17:20:42');
INSERT INTO `sys_oper_log` VALUES ('126', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"updateTime\":1647854863860,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"\",\"createTime\":1647853000000,\"updateBy\":\"\",\"id\":3,\"state\":\"固态\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":102,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 17:27:37');
INSERT INTO `sys_oper_log` VALUES ('127', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"updateTime\":1647854917897,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"\",\"createTime\":1647853000000,\"updateBy\":\"\",\"id\":3,\"state\":\"固态\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":102,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 17:28:31');
INSERT INTO `sys_oper_log` VALUES ('128', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"updateTime\":1647855073609,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":102,\"step\":2},{\"deptId\":109,\"step\":3}],\"createBy\":\"\",\"createTime\":1647854171000,\"updateBy\":\"\",\"id\":4,\"state\":\"对的\",\"config\":\"[{\\\"deptId\\\":102,\\\"step\\\":2},{\\\"deptId\\\":109,\\\"step\\\":3}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 17:31:07');
INSERT INTO `sys_oper_log` VALUES ('129', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":101,\"step\":2}],\"createTime\":1647857809140,\"state\":\"固态\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":101,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 18:16:42');
INSERT INTO `sys_oper_log` VALUES ('130', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"updateTime\":1647857817098,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":101,\"step\":2},{\"deptId\":100,\"step\":3}],\"createBy\":\"\",\"createTime\":1647857809000,\"updateBy\":\"\",\"id\":5,\"state\":\"固态\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":101,\\\"step\\\":2},{\\\"deptId\\\":100,\\\"step\\\":3}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 18:16:50');
INSERT INTO `sys_oper_log` VALUES ('131', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"updateTime\":1647857823004,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":100,\"step\":2}],\"createBy\":\"\",\"createTime\":1647857809000,\"updateBy\":\"\",\"id\":5,\"state\":\"固态\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":100,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 18:16:56');
INSERT INTO `sys_oper_log` VALUES ('132', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"updateTime\":1647857831645,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"\",\"createTime\":1647857809000,\"updateBy\":\"\",\"id\":5,\"state\":\"固态\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 18:17:05');
INSERT INTO `sys_oper_log` VALUES ('133', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":101,\"step\":2}],\"createBy\":\"admin\",\"createTime\":1647865126970,\"state\":\"c\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":101,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:18:44');
INSERT INTO `sys_oper_log` VALUES ('134', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', '1', 'admin', null, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"危险物品形态\",\"params\":{},\"dictType\":\"dict_wastes_state\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:21:11');
INSERT INTO `sys_oper_log` VALUES ('135', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"dict_wastes_state\",\"dictLabel\":\"固态\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:21:49');
INSERT INTO `sys_oper_log` VALUES ('136', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"dict_wastes_state\",\"dictLabel\":\"液态\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:21:57');
INSERT INTO `sys_oper_log` VALUES ('137', '字典数据', '2', 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"dict_wastes_state\",\"dictLabel\":\"液态\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"createTime\":1647865317000,\"dictCode\":101,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:22:05');
INSERT INTO `sys_oper_log` VALUES ('138', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"3\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"dict_wastes_state\",\"dictLabel\":\"气态\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:22:14');
INSERT INTO `sys_oper_log` VALUES ('139', '字典类型', '9', 'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()', 'DELETE', '1', 'admin', null, '/system/dict/type/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:24:15');
INSERT INTO `sys_oper_log` VALUES ('140', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647866069166,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"\",\"createTime\":1647857809000,\"updateBy\":\"admin\",\"id\":5,\"state\":\"固态\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:34:26');
INSERT INTO `sys_oper_log` VALUES ('141', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647866074904,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":101,\"step\":2}],\"createBy\":\"admin\",\"createTime\":1647865127000,\"updateBy\":\"admin\",\"id\":6,\"state\":\"c\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":101,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:34:32');
INSERT INTO `sys_oper_log` VALUES ('142', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647866176303,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"\",\"createTime\":1647857809000,\"updateBy\":\"admin\",\"id\":5,\"state\":\"1\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:36:13');
INSERT INTO `sys_oper_log` VALUES ('143', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647866181360,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":101,\"step\":2}],\"createBy\":\"admin\",\"createTime\":1647865127000,\"updateBy\":\"admin\",\"id\":6,\"state\":\"2\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":101,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 20:36:18');
INSERT INTO `sys_oper_log` VALUES ('144', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647868516267,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"admin\",\"createTime\":1647857809000,\"updateBy\":\"admin\",\"id\":5,\"state\":\"2\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 21:15:14');
INSERT INTO `sys_oper_log` VALUES ('145', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647868611942,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":108,\"step\":1}],\"createBy\":\"admin\",\"createTime\":1647865127000,\"updateBy\":\"admin\",\"id\":6,\"state\":\"3\",\"config\":\"[{\\\"deptId\\\":108,\\\"step\\\":1}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 21:16:49');
INSERT INTO `sys_oper_log` VALUES ('146', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647869481622,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"admin\",\"createTime\":1647857809000,\"updateBy\":\"admin\",\"id\":5,\"state\":\"2\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 21:31:19');
INSERT INTO `sys_oper_log` VALUES ('147', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647869495859,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":100,\"step\":2},{\"deptId\":101,\"step\":3},{\"deptId\":100,\"step\":4},{\"deptId\":100,\"step\":5}],\"createBy\":\"admin\",\"createTime\":1647857809000,\"updateBy\":\"admin\",\"id\":5,\"state\":\"2\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":100,\\\"step\\\":2},{\\\"deptId\\\":101,\\\"step\\\":3},{\\\"deptId\\\":100,\\\"step\\\":4},{\\\"deptId\\\":100,\\\"step\\\":5}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 21:31:33');
INSERT INTO `sys_oper_log` VALUES ('148', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647869506022,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":100,\"step\":2},{\"deptId\":100,\"step\":3},{\"deptId\":100,\"step\":4}],\"createBy\":\"admin\",\"createTime\":1647857809000,\"updateBy\":\"admin\",\"id\":5,\"state\":\"2\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":100,\\\"step\\\":2},{\\\"deptId\\\":100,\\\"step\\\":3},{\\\"deptId\\\":100,\\\"step\\\":4}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 21:31:43');
INSERT INTO `sys_oper_log` VALUES ('149', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647869517427,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":100,\"step\":2},{\"deptId\":100,\"step\":3}],\"createBy\":\"admin\",\"createTime\":1647857809000,\"updateBy\":\"admin\",\"id\":5,\"state\":\"2\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":100,\\\"step\\\":2},{\\\"deptId\\\":100,\\\"step\\\":3}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 21:31:54');
INSERT INTO `sys_oper_log` VALUES ('150', '处理步骤配置', '3', 'com.ruoyi.system.controller.HandleStepConfigController.remove()', 'DELETE', '1', 'admin', null, '/handleStep/6', '127.0.0.1', '内网IP', '{ids=6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 21:38:53');
INSERT INTO `sys_oper_log` VALUES ('151', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":100,\"step\":2},{\"deptId\":100,\"step\":3},{\"deptId\":102,\"step\":4}],\"createBy\":\"admin\",\"createTime\":1647870014926,\"state\":\"1\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":100,\\\"step\\\":2},{\\\"deptId\\\":100,\\\"step\\\":3},{\\\"deptId\\\":102,\\\"step\\\":4}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 21:40:12');
INSERT INTO `sys_oper_log` VALUES ('152', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"admin\",\"state\":\"1\"}', null, '1', '此形态的配置已存在，请勿重复添加！', '2022-03-21 22:05:06');
INSERT INTO `sys_oper_log` VALUES ('153', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"admin\",\"state\":\"1\"}', null, '1', '此形态的配置已存在，请勿重复添加！', '2022-03-21 22:05:10');
INSERT INTO `sys_oper_log` VALUES ('154', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"admin\",\"state\":\"2\"}', null, '1', '此形态的配置已存在，请勿重复添加！', '2022-03-21 22:05:13');
INSERT INTO `sys_oper_log` VALUES ('155', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"admin\",\"createTime\":1647871519689,\"state\":\"3\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 22:05:16');
INSERT INTO `sys_oper_log` VALUES ('156', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647871613104,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":101,\"step\":2},{\"deptId\":102,\"step\":3}],\"createBy\":\"admin\",\"createTime\":1647871520000,\"updateBy\":\"admin\",\"id\":8,\"state\":\"3\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":101,\\\"step\\\":2},{\\\"deptId\\\":102,\\\"step\\\":3}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 22:06:50');
INSERT INTO `sys_oper_log` VALUES ('157', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647871654239,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":100,\"step\":2},{\"deptId\":100,\"step\":3}],\"createBy\":\"admin\",\"createTime\":1647857809000,\"updateBy\":\"admin\",\"id\":5,\"state\":\"2\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":100,\\\"step\\\":2},{\\\"deptId\\\":100,\\\"step\\\":3}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 22:07:31');
INSERT INTO `sys_oper_log` VALUES ('158', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647871662305,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":101,\"step\":2},{\"deptId\":102,\"step\":3}],\"createBy\":\"admin\",\"createTime\":1647871520000,\"updateBy\":\"admin\",\"id\":8,\"state\":\"3\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":101,\\\"step\\\":2},{\\\"deptId\\\":102,\\\"step\\\":3}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 22:07:39');
INSERT INTO `sys_oper_log` VALUES ('159', '处理步骤配置', '3', 'com.ruoyi.system.controller.HandleStepConfigController.remove()', 'DELETE', '1', 'admin', null, '/handleStep/5', '127.0.0.1', '内网IP', '{ids=5}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-21 22:07:53');
INSERT INTO `sys_oper_log` VALUES ('160', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"createUserPhone\":\"15888888888\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"admin\",\"state\":\"3\"}', null, '1', '此形态的配置已存在，请勿重复添加！', '2022-03-22 14:59:37');
INSERT INTO `sys_oper_log` VALUES ('161', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"createUserPhone\":\"15888888888\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1}],\"createBy\":\"admin\",\"createTime\":1647932387782,\"state\":\"2\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 14:59:41');
INSERT INTO `sys_oper_log` VALUES ('162', '代码生成', '6', 'com.ruoyi.generator.controller.GenController.importTableSave()', 'POST', '1', 'admin', null, '/tool/gen/importTable', '127.0.0.1', '内网IP', 't_handle_log', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 15:22:56');
INSERT INTO `sys_oper_log` VALUES ('163', '代码生成', '2', 'com.ruoyi.generator.controller.GenController.editSave()', 'PUT', '1', 'admin', null, '/tool/gen', '127.0.0.1', '内网IP', '{\"sub\":false,\"functionAuthor\":\"wind\",\"columns\":[{\"capJavaField\":\"Id\",\"usableColumn\":false,\"columnId\":32,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"id\",\"htmlType\":\"input\",\"edit\":false,\"query\":false,\"columnComment\":\"主键\",\"sort\":1,\"list\":false,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"1\",\"createTime\":1647933775000,\"tableId\":3,\"pk\":true,\"columnName\":\"id\"},{\"capJavaField\":\"WasteId\",\"usableColumn\":false,\"columnId\":33,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"wasteId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"废物处理id\",\"isQuery\":\"1\",\"sort\":2,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647933775000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"waste_id\"},{\"capJavaField\":\"Step\",\"usableColumn\":false,\"columnId\":34,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"step\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"处理步骤\",\"isQuery\":\"1\",\"sort\":3,\"list\":true,\"params\":{},\"javaType\":\"Integer\",\"queryType\":\"EQ\",\"columnType\":\"int(10)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":1647933775000,\"isEdit\":\"1\",\"tableId\":3,\"pk\":false,\"columnName\":\"step\"},{\"capJavaField\":\"CreateDeptId\",\"usableColumn\":false,\"columnId\":35,\"isIncrement\":\"0\",\"increment\":false,\"insert\":true,\"isList\":\"1\",\"dictType\":\"\",\"required\":false,\"superColumn\":false,\"updateBy\":\"\",\"isInsert\":\"1\",\"javaField\":\"createDeptId\",\"htmlType\":\"input\",\"edit\":true,\"query\":true,\"columnComment\":\"创建部门id\",\"isQuery\":\"1\",\"sort\":4,\"list\":true,\"params\":{},\"javaType\":\"Long\",\"queryType\":\"EQ\",\"columnType\":\"bigint(20)\",\"createBy\":\"admin\",\"isPk\":\"0\",\"createTime\":164', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 15:23:59');
INSERT INTO `sys_oper_log` VALUES ('164', '代码生成', '8', 'com.ruoyi.generator.controller.GenController.batchGenCode()', 'GET', '1', 'admin', null, '/tool/gen/batchGenCode', '127.0.0.1', '内网IP', '{}', null, '0', null, '2022-03-22 15:25:17');
INSERT INTO `sys_oper_log` VALUES ('165', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":3,\"menuName\":\"处置记录\",\"params\":{},\"parentId\":2002,\"isCache\":\"0\",\"path\":\"handlelog\",\"component\":\"handle/log/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 15:29:37');
INSERT INTO `sys_oper_log` VALUES ('166', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":3,\"menuName\":\"处置记录\",\"params\":{},\"parentId\":2002,\"isCache\":\"0\",\"path\":\"handlelog\",\"component\":\"handle/log/index\",\"children\":[],\"createTime\":1647934176000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2004,\"menuType\":\"C\",\"perms\":\"handle:log:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 15:33:56');
INSERT INTO `sys_oper_log` VALUES ('167', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"num\":\"1\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"1\",\"monthly\":\"2022-1\",\"name\":\"1\",\"createById\":1,\"state\":\"1\",\"plan\":\"1\"}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-22 16:36:44');
INSERT INTO `sys_oper_log` VALUES ('168', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"num\":\"1\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"1\",\"monthly\":\"2022-1\",\"name\":\"1\",\"createById\":1,\"state\":\"2\",\"plan\":\"1\"}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-22 16:36:54');
INSERT INTO `sys_oper_log` VALUES ('169', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"num\":\"1\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"1\",\"monthly\":\"2022-1\",\"name\":\"1\",\"createById\":1,\"state\":\"2\",\"plan\":\"1\"}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-22 16:38:23');
INSERT INTO `sys_oper_log` VALUES ('170', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"1\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"1\",\"createTime\":1647938410079,\"handleDeptId\":100,\"monthly\":\"2022-1\",\"name\":\"1\",\"createById\":1,\"id\":2,\"state\":\"2\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 16:40:03');
INSERT INTO `sys_oper_log` VALUES ('171', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"1\",\"createUserPhone\":\"15888888888\",\"updateTime\":1647938451774,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"1\",\"createTime\":1647938410000,\"updateBy\":\"admin\",\"handleDeptId\":100,\"monthly\":\"2022-1\",\"name\":\"1\",\"createById\":1,\"id\":2,\"state\":\"2\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 16:40:45');
INSERT INTO `sys_oper_log` VALUES ('172', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"cascader\",\"orderNum\":2,\"menuName\":\"处置管理\",\"params\":{},\"parentId\":2002,\"isCache\":\"0\",\"path\":\"handleWaste\",\"component\":\"wasteDisposal/info/handle\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:07:37');
INSERT INTO `sys_oper_log` VALUES ('173', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/2', '127.0.0.1', '内网IP', '2', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:15:07');
INSERT INTO `sys_oper_log` VALUES ('174', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"10\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"吨\",\"createTime\":1647955052440,\"handleDeptId\":100,\"monthly\":\"2022-01\",\"name\":\"危险品固态测试\",\"createById\":1,\"id\":3,\"state\":\"1\",\"plan\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:17:29');
INSERT INTO `sys_oper_log` VALUES ('175', '处理步骤配置', '2', 'com.ruoyi.system.controller.HandleStepConfigController.edit()', 'PUT', '1', 'admin', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"updateTime\":1647955247893,\"delFlag\":\"0\",\"params\":{},\"handleStepList\":[{\"deptId\":100,\"step\":1},{\"deptId\":101,\"step\":2},{\"deptId\":109,\"step\":3},{\"deptId\":102,\"step\":4}],\"createBy\":\"admin\",\"createTime\":1647870015000,\"updateBy\":\"admin\",\"id\":7,\"state\":\"1\",\"config\":\"[{\\\"deptId\\\":100,\\\"step\\\":1},{\\\"deptId\\\":101,\\\"step\\\":2},{\\\"deptId\\\":109,\\\"step\\\":3},{\\\"deptId\\\":102,\\\"step\\\":4}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:20:44');
INSERT INTO `sys_oper_log` VALUES ('176', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/3', '127.0.0.1', '内网IP', '3', null, '1', '\r\n### Error updating database.  Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\r\n### The error may exist in file [D:\\WorkSpace\\waste-disposal\\ruoyi-system\\target\\classes\\mapper\\handle\\HandleLogMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.HandleLogMapper.insertHandleLog-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_handle_log          ( waste_id,             step,             create_dept_id,             create_dept_name,             create_by,             create_time,             create_user_phone )           values ( ?,             ?,             ?,             ?,             ?,             ?,             ? )\r\n### Cause: java.sql.SQLException: Field \'id\' doesn\'t have a default value\n; Field \'id\' doesn\'t have a default value; nested exception is java.sql.SQLException: Field \'id\' doesn\'t have a default value', '2022-03-22 21:20:53');
INSERT INTO `sys_oper_log` VALUES ('177', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/3', '127.0.0.1', '内网IP', '3', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:21:25');
INSERT INTO `sys_oper_log` VALUES ('178', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/3', '127.0.0.1', '内网IP', '3', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:22:04');
INSERT INTO `sys_oper_log` VALUES ('179', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/3', '127.0.0.1', '内网IP', '3', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:23:19');
INSERT INTO `sys_oper_log` VALUES ('180', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/3', '127.0.0.1', '内网IP', '3', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:23:33');
INSERT INTO `sys_oper_log` VALUES ('181', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/2004', '127.0.0.1', '内网IP', '{menuId=2004}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:40:21');
INSERT INTO `sys_oper_log` VALUES ('182', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/3', '127.0.0.1', '内网IP', '3', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 21:40:38');
INSERT INTO `sys_oper_log` VALUES ('183', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/1', '127.0.0.1', '内网IP', '1', null, '1', '', '2022-03-22 22:31:15');
INSERT INTO `sys_oper_log` VALUES ('184', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/1', '127.0.0.1', '内网IP', '1', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-22 22:32:23');
INSERT INTO `sys_oper_log` VALUES ('185', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":1,\"menuName\":\"产废信息管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/info\",\"component\":\"wasteDisposal/info/index.vue\",\"children\":[],\"createTime\":1647687339000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:15:18');
INSERT INTO `sys_oper_log` VALUES ('186', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.report()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/report/1', '127.0.0.1', '内网IP', '1', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:35:55');
INSERT INTO `sys_oper_log` VALUES ('187', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.report()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/report/2', '127.0.0.1', '内网IP', '2', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:37:49');
INSERT INTO `sys_oper_log` VALUES ('188', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', '1', 'admin', null, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"常用单位\",\"params\":{},\"dictType\":\"ditc_unit_type\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:44:40');
INSERT INTO `sys_oper_log` VALUES ('189', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"g\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"ditc_unit_type\",\"dictLabel\":\"克\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:45:04');
INSERT INTO `sys_oper_log` VALUES ('190', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"kg\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"ditc_unit_type\",\"dictLabel\":\"千克\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:45:22');
INSERT INTO `sys_oper_log` VALUES ('191', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"k\",\"listClass\":\"default\",\"dictSort\":2,\"params\":{},\"dictType\":\"ditc_unit_type\",\"dictLabel\":\"吨\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:45:39');
INSERT INTO `sys_oper_log` VALUES ('192', '字典类型', '2', 'com.ruoyi.web.controller.system.SysDictTypeController.edit()', 'PUT', '1', 'admin', null, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"createTime\":1647999880000,\"updateBy\":\"admin\",\"dictName\":\"常用单位\",\"dictId\":101,\"params\":{},\"dictType\":\"dict_unit_type\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:46:22');
INSERT INTO `sys_oper_log` VALUES ('193', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"num\":\"465.5\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"k\",\"monthly\":\"2021-12-31T16:00:00.000Z\",\"name\":\"测试\",\"createById\":1}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-23 09:53:32');
INSERT INTO `sys_oper_log` VALUES ('194', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"num\":\"465.5\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"k\",\"monthly\":\"2021-12-31T16:00:00.000Z\",\"name\":\"测试\",\"createById\":1}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-23 09:53:39');
INSERT INTO `sys_oper_log` VALUES ('195', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"num\":\"465.5\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"k\",\"monthly\":\"2021-12-31T16:00:00.000Z\",\"name\":\"测试\",\"createById\":1}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-23 09:53:42');
INSERT INTO `sys_oper_log` VALUES ('196', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"num\":\"465.5\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"k\",\"monthly\":\"2021-12-31T16:00:00.000Z\",\"name\":\"测试\",\"createById\":1}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-23 09:53:47');
INSERT INTO `sys_oper_log` VALUES ('197', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"num\":\"465.5\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"k\",\"monthly\":\"2021-12-31T16:00:00.000Z\",\"name\":\"测试\",\"createById\":1}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-23 09:53:50');
INSERT INTO `sys_oper_log` VALUES ('198', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"155.1\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"k\",\"createTime\":1648000497269,\"handleDeptId\":100,\"monthly\":\"2022-01-31T16:00:00.000Z\",\"name\":\"测试\",\"report\":\"0\",\"createById\":1,\"state\":\"1\",\"complete\":\"0\"}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'monthly\' at row 1\r\n### The error may exist in file [E:\\workspace\\RuoYi-Vue\\ruoyi-system\\target\\classes\\mapper\\wasteDisposal\\WasteDisposalInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.WasteDisposalInfoMapper.insertWasteDisposalInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_waste_disposal_info          ( name,             num,             unit,                          state,             monthly,             complete,             report,             create_dept_id,             create_by_id,             create_dept_name,             create_by,             create_time,                                       create_user_phone,             handle_step,             handle_dept_id )           values ( ?,             ?,             ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                       ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'monthly\' at row 1\n; Data truncation: Data too long for column \'monthly\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'monthly\' at row 1', '2022-03-23 09:54:57');
INSERT INTO `sys_oper_log` VALUES ('199', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"15.6\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"kg\",\"createTime\":1648000572690,\"handleDeptId\":100,\"monthly\":\"2022-02-28T16:00:00.000Z\",\"name\":\"测试\",\"report\":\"0\",\"createById\":1,\"state\":\"2\",\"complete\":\"0\"}', null, '1', '\r\n### Error updating database.  Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'monthly\' at row 1\r\n### The error may exist in file [E:\\workspace\\RuoYi-Vue\\ruoyi-system\\target\\classes\\mapper\\wasteDisposal\\WasteDisposalInfoMapper.xml]\r\n### The error may involve com.ruoyi.system.mapper.WasteDisposalInfoMapper.insertWasteDisposalInfo-Inline\r\n### The error occurred while setting parameters\r\n### SQL: insert into t_waste_disposal_info          ( name,             num,             unit,                          state,             monthly,             complete,             report,             create_dept_id,             create_by_id,             create_dept_name,             create_by,             create_time,                                       create_user_phone,             handle_step,             handle_dept_id )           values ( ?,             ?,             ?,                          ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,             ?,                                       ?,             ?,             ? )\r\n### Cause: com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'monthly\' at row 1\n; Data truncation: Data too long for column \'monthly\' at row 1; nested exception is com.mysql.cj.jdbc.exceptions.MysqlDataTruncation: Data truncation: Data too long for column \'monthly\' at row 1', '2022-03-23 09:56:12');
INSERT INTO `sys_oper_log` VALUES ('200', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"123.5\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"k\",\"createTime\":1648000645277,\"handleDeptId\":100,\"monthly\":\"2022-03\",\"name\":\"测试\",\"report\":\"0\",\"createById\":1,\"id\":4,\"state\":\"1\",\"complete\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 09:57:24');
INSERT INTO `sys_oper_log` VALUES ('201', '字典类型', '9', 'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()', 'DELETE', '1', 'admin', null, '/system/dict/type/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:00:23');
INSERT INTO `sys_oper_log` VALUES ('202', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"1\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"g\",\"createTime\":1648000990928,\"handleDeptId\":100,\"monthly\":\"2022-10\",\"name\":\"测试\",\"report\":\"0\",\"createById\":1,\"id\":5,\"state\":\"2\",\"complete\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:03:10');
INSERT INTO `sys_oper_log` VALUES ('203', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"1\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"g\",\"createTime\":1648001056431,\"handleDeptId\":100,\"monthly\":\"2022-10\",\"name\":\"尺寸\",\"report\":\"0\",\"createById\":1,\"id\":6,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:04:16');
INSERT INTO `sys_oper_log` VALUES ('204', '危险废物处置信息', '3', 'com.ruoyi.system.controller.WasteDisposalInfoController.remove()', 'DELETE', '1', 'admin', null, '/wasteDisposal/info/6', '127.0.0.1', '内网IP', '{ids=6}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:07:14');
INSERT INTO `sys_oper_log` VALUES ('205', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":4,\"num\":\"10\",\"createUserPhone\":\"15888888888\",\"updateTime\":1648001246322,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"k\",\"createTime\":1647955052000,\"updateBy\":\"admin\",\"handleDeptId\":102,\"monthly\":\"2022-01\",\"name\":\"危险品固态测试\",\"createById\":1,\"id\":3,\"state\":\"2\",\"complete\":\"1\",\"plan\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:07:25');
INSERT INTO `sys_oper_log` VALUES ('206', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.report()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/report/3', '127.0.0.1', '内网IP', '3', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:07:27');
INSERT INTO `sys_oper_log` VALUES ('207', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'admin', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"handleStep\":1,\"num\":\"23\",\"createUserPhone\":\"15888888888\",\"params\":{},\"createBy\":\"admin\",\"unit\":\"g\",\"createTime\":1648001283225,\"handleDeptId\":100,\"monthly\":\"2022-11\",\"name\":\"踩踩踩\",\"report\":\"0\",\"createById\":1,\"id\":7,\"state\":\"2\",\"complete\":\"0\",\"plan\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:08:02');
INSERT INTO `sys_oper_log` VALUES ('208', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"druid\",\"orderNum\":1,\"menuName\":\"计划上报\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/plan\",\"component\":\"wasteDisposal/info/plan\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:16:54');
INSERT INTO `sys_oper_log` VALUES ('209', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":2,\"menuName\":\"产废信息管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/info\",\"component\":\"wasteDisposal/info/index.vue\",\"children\":[],\"createTime\":1647687339000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:19:06');
INSERT INTO `sys_oper_log` VALUES ('210', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"tool\",\"orderNum\":3,\"menuName\":\"环保局\",\"params\":{},\"parentId\":0,\"isCache\":\"0\",\"path\":\"EPA\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"M\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:53:23');
INSERT INTO `sys_oper_log` VALUES ('211', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":1,\"menuName\":\"处置中心\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"handleAll\",\"component\":\"wasteDisposal/info/handleAll\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:55:42');
INSERT INTO `sys_oper_log` VALUES ('212', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":2,\"menuName\":\"计划比对\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"handleNotify\",\"component\":\"wasteDisposal/info/handleNotify\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 10:56:46');
INSERT INTO `sys_oper_log` VALUES ('213', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.notify()', 'POST', '1', 'admin', null, '/wasteDisposal/info/notify', '127.0.0.1', '内网IP', '', null, '1', '', '2022-03-23 15:02:45');
INSERT INTO `sys_oper_log` VALUES ('214', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.notify()', 'POST', '1', 'admin', null, '/wasteDisposal/info/notify', '127.0.0.1', '内网IP', '{\"id\":3,\"notifyType\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 15:10:02');
INSERT INTO `sys_oper_log` VALUES ('215', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.notify()', 'POST', '1', 'admin', null, '/wasteDisposal/info/notify', '127.0.0.1', '内网IP', '{\"id\":2,\"notifyType\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 15:13:52');
INSERT INTO `sys_oper_log` VALUES ('216', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'admin', null, '/wasteDisposal/info/handleWaste/1', '127.0.0.1', '内网IP', '1', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 16:50:11');
INSERT INTO `sys_oper_log` VALUES ('217', '字典数据', '2', 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"1\",\"listClass\":\"success\",\"dictSort\":1,\"remark\":\"通知\",\"params\":{},\"dictType\":\"sys_notice_type\",\"dictLabel\":\"通过\",\"createBy\":\"admin\",\"default\":true,\"isDefault\":\"Y\",\"cssClass\":\"\",\"createTime\":1647583185000,\"dictCode\":14,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 16:55:36');
INSERT INTO `sys_oper_log` VALUES ('218', '字典数据', '2', 'com.ruoyi.web.controller.system.SysDictDataController.edit()', 'PUT', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"2\",\"listClass\":\"warning\",\"dictSort\":2,\"remark\":\"公告\",\"params\":{},\"dictType\":\"sys_notice_type\",\"dictLabel\":\"警告\",\"createBy\":\"admin\",\"default\":false,\"isDefault\":\"N\",\"cssClass\":\"\",\"createTime\":1647583185000,\"dictCode\":15,\"updateBy\":\"admin\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 16:55:47');
INSERT INTO `sys_oper_log` VALUES ('219', '字典类型', '9', 'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()', 'DELETE', '1', 'admin', null, '/system/dict/type/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 16:56:04');
INSERT INTO `sys_oper_log` VALUES ('220', '通知公告', '1', 'com.ruoyi.web.controller.system.SysNoticeController.add()', 'POST', '1', 'admin', null, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"1\",\"params\":{},\"noticeTitle\":\"测试\",\"createBy\":\"admin\",\"receiveDeptId\":100,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:19:57');
INSERT INTO `sys_oper_log` VALUES ('221', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', '1', 'admin', null, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"1\",\"params\":{},\"noticeId\":11,\"noticeTitle\":\"测试\",\"createBy\":\"admin\",\"createTime\":1648027197000,\"updateBy\":\"admin\",\"receiveDeptId\":100,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:20:56');
INSERT INTO `sys_oper_log` VALUES ('222', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', '1', 'admin', null, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"1\",\"updateTime\":1648027256000,\"params\":{},\"noticeId\":11,\"noticeTitle\":\"测试\",\"createBy\":\"admin\",\"createTime\":1648027197000,\"updateBy\":\"admin\",\"receiveDeptId\":100,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:22:36');
INSERT INTO `sys_oper_log` VALUES ('223', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', '1', 'admin', null, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"1\",\"updateTime\":1648027355000,\"params\":{},\"noticeId\":11,\"noticeTitle\":\"测试\",\"createBy\":\"admin\",\"createTime\":1648027197000,\"updateBy\":\"admin\",\"receiveDeptName\":\"长沙分公司\",\"receiveDeptId\":102,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:22:59');
INSERT INTO `sys_oper_log` VALUES ('224', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', '1', 'admin', null, '/system/notice', '127.0.0.1', '内网IP', '{\"noticeType\":\"1\",\"updateTime\":1648027378000,\"params\":{},\"noticeId\":11,\"noticeTitle\":\"测试\",\"createBy\":\"admin\",\"createTime\":1648027197000,\"updateBy\":\"admin\",\"receiveDeptName\":\"长沙分公司\",\"receiveDeptId\":102,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:23:13');
INSERT INTO `sys_oper_log` VALUES ('225', '通知公告', '2', 'com.ruoyi.web.controller.system.SysNoticeController.edit()', 'PUT', '1', 'admin', null, '/system/notice', '127.0.0.1', '内网IP', '{\"createDeptId\":103,\"createDeptName\":\"研发部门\",\"noticeType\":\"1\",\"params\":{},\"noticeId\":10,\"noticeTitle\":\"2022-01月度液态废物处理全部完成\",\"createBy\":\"admin\",\"createTime\":1648025411000,\"updateBy\":\"admin\",\"receiveDeptName\":\"深圳总公司\",\"receiveDeptId\":101,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:26:17');
INSERT INTO `sys_oper_log` VALUES ('226', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"wechat\",\"orderNum\":1,\"menuName\":\"通知公告\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"/notice/receive\",\"component\":\"system/notice/receive\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:43:53');
INSERT INTO `sys_oper_log` VALUES ('227', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"druid\",\"orderNum\":2,\"menuName\":\"计划上报\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/plan\",\"component\":\"wasteDisposal/info/plan\",\"children\":[],\"createTime\":1648001814000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2006,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:44:01');
INSERT INTO `sys_oper_log` VALUES ('228', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"druid\",\"orderNum\":3,\"menuName\":\"计划上报\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/plan\",\"component\":\"wasteDisposal/info/plan\",\"children\":[],\"createTime\":1648001814000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2006,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:44:05');
INSERT INTO `sys_oper_log` VALUES ('229', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"wechat\",\"orderNum\":1,\"menuName\":\"通知公告\",\"params\":{},\"parentId\":2002,\"isCache\":\"0\",\"path\":\"/notice/index\",\"component\":\"system/notice/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:45:40');
INSERT INTO `sys_oper_log` VALUES ('230', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"system\",\"orderNum\":3,\"menuName\":\"处置步骤\",\"params\":{},\"parentId\":2002,\"isCache\":\"0\",\"path\":\"handleStep\",\"component\":\"handleStep/config/index\",\"children\":[],\"createTime\":1647844898000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2003,\"menuType\":\"C\",\"perms\":\"handleStep:config:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:45:47');
INSERT INTO `sys_oper_log` VALUES ('231', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"wechat\",\"orderNum\":1,\"menuName\":\"通知公告\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"/notice/index\",\"component\":\"system/notice/index\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:46:45');
INSERT INTO `sys_oper_log` VALUES ('232', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":2,\"menuName\":\"处置中心\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"handleAll\",\"component\":\"wasteDisposal/info/handleAll\",\"children\":[],\"createTime\":1648004142000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2008,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:48:35');
INSERT INTO `sys_oper_log` VALUES ('233', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"clipboard\",\"orderNum\":3,\"menuName\":\"计划比对\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"handleNotify\",\"component\":\"wasteDisposal/info/handleNotify\",\"children\":[],\"createTime\":1648004206000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2009,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:48:40');
INSERT INTO `sys_oper_log` VALUES ('234', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"wechat\",\"orderNum\":1,\"menuName\":\"通知公告\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"EPA/notice/index\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1648028805000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2012,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:48:58');
INSERT INTO `sys_oper_log` VALUES ('235', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":3,\"menuName\":\"产废信息管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/info\",\"component\":\"wasteDisposal/info/index.vue\",\"children\":[],\"createTime\":1647687339000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:50:13');
INSERT INTO `sys_oper_log` VALUES ('236', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":2,\"menuName\":\"产废信息管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/info\",\"component\":\"wasteDisposal/info/index.vue\",\"children\":[],\"createTime\":1647687339000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:50:25');
INSERT INTO `sys_oper_log` VALUES ('237', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"chart\",\"orderNum\":3,\"menuName\":\"产废信息管理\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/info\",\"component\":\"wasteDisposal/info/index.vue\",\"children\":[],\"createTime\":1647687339000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2001,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:50:31');
INSERT INTO `sys_oper_log` VALUES ('238', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"druid\",\"orderNum\":2,\"menuName\":\"计划上报\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"wasteDisposal/plan\",\"component\":\"wasteDisposal/info/plan\",\"children\":[],\"createTime\":1648001814000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2006,\"menuType\":\"C\",\"perms\":\"wasteDisposal:info:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:50:37');
INSERT INTO `sys_oper_log` VALUES ('239', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":1,\"menuName\":\"通知公告\",\"params\":{},\"parentId\":2000,\"isCache\":\"0\",\"path\":\"/notice/receive\",\"component\":\"system/notice/receive\",\"children\":[],\"createTime\":1648028633000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2010,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:51:28');
INSERT INTO `sys_oper_log` VALUES ('240', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":1,\"menuName\":\"通知公告\",\"params\":{},\"parentId\":2002,\"isCache\":\"0\",\"path\":\"/notice/index\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1648028740000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2011,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:51:43');
INSERT INTO `sys_oper_log` VALUES ('241', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"message\",\"orderNum\":1,\"menuName\":\"通知公告\",\"params\":{},\"parentId\":2007,\"isCache\":\"0\",\"path\":\"EPA/notice/index\",\"component\":\"system/notice/index\",\"children\":[],\"createTime\":1648028805000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2012,\"menuType\":\"C\",\"perms\":\"system:notice:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:51:57');
INSERT INTO `sys_oper_log` VALUES ('242', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"通知查询\",\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"system:notice:query\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:59:37');
INSERT INTO `sys_oper_log` VALUES ('243', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":2,\"menuName\":\"通知新增\",\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"system:notice:add\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 17:59:56');
INSERT INTO `sys_oper_log` VALUES ('244', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":3,\"menuName\":\"通知修改\",\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"system:notice:edit\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:00:16');
INSERT INTO `sys_oper_log` VALUES ('245', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":4,\"menuName\":\"通知删除\",\"params\":{},\"parentId\":2012,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"system:notice:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:00:44');
INSERT INTO `sys_oper_log` VALUES ('246', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"通知查询\",\"params\":{},\"parentId\":2011,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"system:notice:query\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:01:10');
INSERT INTO `sys_oper_log` VALUES ('247', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":2,\"menuName\":\"通知新增\",\"params\":{},\"parentId\":2011,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"system:notice:add\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:01:31');
INSERT INTO `sys_oper_log` VALUES ('248', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":3,\"menuName\":\"通知编辑\",\"params\":{},\"parentId\":2011,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"system:notice:edit\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:01:57');
INSERT INTO `sys_oper_log` VALUES ('249', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":4,\"menuName\":\"通知删除\",\"params\":{},\"parentId\":2011,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"system:notice:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:02:20');
INSERT INTO `sys_oper_log` VALUES ('250', '菜单管理', '3', 'com.ruoyi.web.controller.system.SysMenuController.remove()', 'DELETE', '1', 'admin', null, '/system/menu/4', '127.0.0.1', '内网IP', '{menuId=4}', '{\"msg\":\"菜单已分配,不允许删除\",\"code\":500}', '0', null, '2022-03-23 18:02:28');
INSERT INTO `sys_oper_log` VALUES ('251', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"查询\",\"params\":{},\"parentId\":2006,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:query\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:06:02');
INSERT INTO `sys_oper_log` VALUES ('252', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":2,\"menuName\":\"新增\",\"params\":{},\"parentId\":2006,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:add\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:06:22');
INSERT INTO `sys_oper_log` VALUES ('253', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":3,\"menuName\":\"编辑\",\"params\":{},\"parentId\":2006,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:edit\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:06:44');
INSERT INTO `sys_oper_log` VALUES ('254', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":5,\"menuName\":\"删除\",\"params\":{},\"parentId\":2006,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:07:03');
INSERT INTO `sys_oper_log` VALUES ('255', '菜单管理', '2', 'com.ruoyi.web.controller.system.SysMenuController.edit()', 'PUT', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"icon\":\"#\",\"orderNum\":4,\"menuName\":\"删除\",\"params\":{},\"parentId\":2006,\"isCache\":\"0\",\"path\":\"\",\"children\":[],\"createTime\":1648030023000,\"updateBy\":\"admin\",\"isFrame\":\"1\",\"menuId\":2024,\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:07:12');
INSERT INTO `sys_oper_log` VALUES ('256', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":4,\"menuName\":\"上报\",\"params\":{},\"parentId\":2006,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:report\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:08:04');
INSERT INTO `sys_oper_log` VALUES ('257', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"查询\",\"params\":{},\"parentId\":2001,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:query\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:08:42');
INSERT INTO `sys_oper_log` VALUES ('258', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":2,\"menuName\":\"新增\",\"params\":{},\"parentId\":2001,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:add\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:09:12');
INSERT INTO `sys_oper_log` VALUES ('259', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":3,\"menuName\":\"编辑\",\"params\":{},\"parentId\":2001,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:edit\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:09:32');
INSERT INTO `sys_oper_log` VALUES ('260', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":4,\"menuName\":\"删除\",\"params\":{},\"parentId\":2001,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:09:55');
INSERT INTO `sys_oper_log` VALUES ('261', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":5,\"menuName\":\"上报\",\"params\":{},\"parentId\":2001,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:report\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:10:13');
INSERT INTO `sys_oper_log` VALUES ('262', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"查询\",\"params\":{},\"parentId\":2005,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:query\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:11:46');
INSERT INTO `sys_oper_log` VALUES ('263', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":2,\"menuName\":\"处理\",\"params\":{},\"parentId\":2005,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:handleWaste\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:12:16');
INSERT INTO `sys_oper_log` VALUES ('264', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":3,\"menuName\":\"处置记录\",\"params\":{},\"parentId\":2005,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"handle:log:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:13:18');
INSERT INTO `sys_oper_log` VALUES ('265', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"查询\",\"params\":{},\"parentId\":2003,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"handleStep:config:query\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:13:47');
INSERT INTO `sys_oper_log` VALUES ('266', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":2,\"menuName\":\"新增\",\"params\":{},\"parentId\":2003,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"handleStep:config:add\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:14:07');
INSERT INTO `sys_oper_log` VALUES ('267', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":3,\"menuName\":\"修改\",\"params\":{},\"parentId\":2003,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"handleStep:config:edit\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:14:28');
INSERT INTO `sys_oper_log` VALUES ('268', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":4,\"menuName\":\"删除\",\"params\":{},\"parentId\":2003,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"handleStep:config:remove\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:14:51');
INSERT INTO `sys_oper_log` VALUES ('269', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"查询\",\"params\":{},\"parentId\":2008,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:query\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:15:55');
INSERT INTO `sys_oper_log` VALUES ('270', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":2,\"menuName\":\"处置记录\",\"params\":{},\"parentId\":2008,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"handle:log:list\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:16:17');
INSERT INTO `sys_oper_log` VALUES ('271', '菜单管理', '1', 'com.ruoyi.web.controller.system.SysMenuController.add()', 'POST', '1', 'admin', null, '/system/menu', '127.0.0.1', '内网IP', '{\"visible\":\"0\",\"orderNum\":1,\"menuName\":\"计划比对\",\"params\":{},\"parentId\":2009,\"isCache\":\"0\",\"createBy\":\"admin\",\"children\":[],\"isFrame\":\"1\",\"menuType\":\"F\",\"perms\":\"wasteDisposal:info:compareList\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:17:18');
INSERT INTO `sys_oper_log` VALUES ('272', '用户管理', '3', 'com.ruoyi.web.controller.system.SysUserController.remove()', 'DELETE', '1', 'admin', null, '/system/user/2', '127.0.0.1', '内网IP', '{userIds=2}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:17:49');
INSERT INTO `sys_oper_log` VALUES ('273', '部门管理', '2', 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', '1', 'admin', null, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"环保局\",\"leader\":\"若依\",\"deptId\":100,\"orderNum\":\"0\",\"delFlag\":\"0\",\"params\":{},\"parentId\":0,\"createBy\":\"admin\",\"children\":[],\"createTime\":1647583185000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0\",\"email\":\"ry@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:18:47');
INSERT INTO `sys_oper_log` VALUES ('274', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":1,\"admin\":true,\"dataScope\":\"1\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"status\":\"0\"}],\"phonenumber\":\"15888888888\",\"admin\":true,\"loginDate\":1648028949000,\"remark\":\"管理员\",\"delFlag\":\"0\",\"password\":\"\",\"postIds\":[1],\"loginIp\":\"127.0.0.1\",\"email\":\"ry@163.com\",\"nickName\":\"若依\",\"sex\":\"1\",\"deptId\":100,\"avatar\":\"\",\"dept\":{\"deptName\":\"研发部门\",\"leader\":\"若依\",\"deptId\":103,\"orderNum\":\"1\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"admin\",\"userId\":1,\"createBy\":\"admin\",\"roleIds\":[1],\"createTime\":1647583185000,\"status\":\"0\"}', null, '1', '不允许操作超级管理员用户', '2022-03-23 18:19:04');
INSERT INTO `sys_oper_log` VALUES ('275', '用户管理', '2', 'com.ruoyi.web.controller.system.SysUserController.edit()', 'PUT', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"roles\":[{\"flag\":false,\"roleId\":1,\"admin\":true,\"dataScope\":\"1\",\"params\":{},\"roleSort\":\"1\",\"deptCheckStrictly\":false,\"menuCheckStrictly\":false,\"roleKey\":\"admin\",\"roleName\":\"超级管理员\",\"status\":\"0\"}],\"phonenumber\":\"15888888888\",\"admin\":true,\"loginDate\":1648028949000,\"remark\":\"管理员\",\"delFlag\":\"0\",\"password\":\"\",\"postIds\":[1],\"loginIp\":\"127.0.0.1\",\"email\":\"ry@163.com\",\"nickName\":\"若依\",\"sex\":\"1\",\"deptId\":100,\"avatar\":\"\",\"dept\":{\"deptName\":\"研发部门\",\"leader\":\"若依\",\"deptId\":103,\"orderNum\":\"1\",\"params\":{},\"parentId\":101,\"children\":[],\"status\":\"0\"},\"params\":{},\"userName\":\"admin\",\"userId\":1,\"createBy\":\"admin\",\"roleIds\":[1],\"createTime\":1647583185000,\"status\":\"0\"}', null, '1', '不允许操作超级管理员用户', '2022-03-23 18:19:12');
INSERT INTO `sys_oper_log` VALUES ('276', '部门管理', '2', 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', '1', 'admin', null, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"石嘴山环保局\",\"leader\":\"若依\",\"deptId\":101,\"orderNum\":\"1\",\"delFlag\":\"0\",\"params\":{},\"parentId\":100,\"createBy\":\"admin\",\"children\":[],\"createTime\":1647583185000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100\",\"email\":\"ry@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:20:16');
INSERT INTO `sys_oper_log` VALUES ('277', '部门管理', '3', 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'DELETE', '1', 'admin', null, '/system/dept/104', '127.0.0.1', '内网IP', '{deptId=104}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:20:24');
INSERT INTO `sys_oper_log` VALUES ('278', '部门管理', '3', 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'DELETE', '1', 'admin', null, '/system/dept/105', '127.0.0.1', '内网IP', '{deptId=105}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:20:27');
INSERT INTO `sys_oper_log` VALUES ('279', '部门管理', '3', 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'DELETE', '1', 'admin', null, '/system/dept/106', '127.0.0.1', '内网IP', '{deptId=106}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:20:30');
INSERT INTO `sys_oper_log` VALUES ('280', '部门管理', '3', 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'DELETE', '1', 'admin', null, '/system/dept/107', '127.0.0.1', '内网IP', '{deptId=107}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:20:33');
INSERT INTO `sys_oper_log` VALUES ('281', '部门管理', '3', 'com.ruoyi.web.controller.system.SysDeptController.remove()', 'DELETE', '1', 'admin', null, '/system/dept/102', '127.0.0.1', '内网IP', '{deptId=102}', '{\"msg\":\"存在下级部门,不允许删除\",\"code\":500}', '0', null, '2022-03-23 18:20:36');
INSERT INTO `sys_oper_log` VALUES ('282', '部门管理', '2', 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', '1', 'admin', null, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"处置中心\",\"leader\":\"若依\",\"deptId\":102,\"orderNum\":\"2\",\"delFlag\":\"0\",\"params\":{},\"parentId\":100,\"createBy\":\"admin\",\"children\":[],\"createTime\":1647583185000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100\",\"email\":\"ry@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:20:52');
INSERT INTO `sys_oper_log` VALUES ('283', '部门管理', '2', 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', '1', 'admin', null, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"焚烧部门\",\"leader\":\"若依\",\"deptId\":108,\"orderNum\":\"1\",\"delFlag\":\"0\",\"params\":{},\"parentId\":102,\"createBy\":\"admin\",\"children\":[],\"createTime\":1647583185000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100,102\",\"email\":\"ry@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:21:06');
INSERT INTO `sys_oper_log` VALUES ('284', '部门管理', '2', 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', '1', 'admin', null, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"掩埋部门\",\"leader\":\"若依\",\"deptId\":109,\"orderNum\":\"2\",\"delFlag\":\"0\",\"params\":{},\"parentId\":102,\"createBy\":\"admin\",\"children\":[],\"createTime\":1647583185000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"ancestors\":\"0,100,102\",\"email\":\"ry@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:21:14');
INSERT INTO `sys_oper_log` VALUES ('285', '部门管理', '1', 'com.ruoyi.web.controller.system.SysDeptController.add()', 'POST', '1', 'admin', null, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"石嘴山XX洗煤厂\",\"leader\":\"张三\",\"orderNum\":\"3\",\"params\":{},\"parentId\":100,\"createBy\":\"admin\",\"children\":[],\"phone\":\"18888888888\",\"ancestors\":\"0,100\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:21:59');
INSERT INTO `sys_oper_log` VALUES ('286', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"普通角色\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1647583185000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"company\",\"roleName\":\"企业\",\"menuIds\":[2000,2010,2006,2021,2022,2023,2024,2025,2001,2026,2027,2028,2029,2030],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:22:54');
INSERT INTO `sys_oper_log` VALUES ('287', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":2,\"admin\":false,\"remark\":\"企业\",\"dataScope\":\"2\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createTime\":1647583185000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"company\",\"roleName\":\"企业\",\"menuIds\":[2000,2010,2006,2021,2022,2023,2024,2025,2001,2026,2027,2028,2029,2030],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:23:01');
INSERT INTO `sys_oper_log` VALUES ('288', '角色管理', '1', 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"remark\":\"处置中心\",\"params\":{},\"roleSort\":\"2\",\"deptCheckStrictly\":true,\"createBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"disposalCenter\",\"roleName\":\"处置中心\",\"deptIds\":[],\"menuIds\":[2002,2011,2017,2018,2019,2020,2005,2031,2032,2033,2003,2034,2035,2036,2037],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:23:55');
INSERT INTO `sys_oper_log` VALUES ('289', '角色管理', '2', 'com.ruoyi.web.controller.system.SysRoleController.edit()', 'PUT', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":100,\"admin\":false,\"remark\":\"处置中心\",\"dataScope\":\"1\",\"delFlag\":\"0\",\"params\":{},\"roleSort\":\"3\",\"deptCheckStrictly\":true,\"createTime\":1648031035000,\"updateBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"disposalCenter\",\"roleName\":\"处置中心\",\"menuIds\":[2002,2011,2017,2018,2019,2020,2005,2031,2032,2033,2003,2034,2035,2036,2037],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:24:06');
INSERT INTO `sys_oper_log` VALUES ('290', '角色管理', '1', 'com.ruoyi.web.controller.system.SysRoleController.add()', 'POST', '1', 'admin', null, '/system/role', '127.0.0.1', '内网IP', '{\"flag\":false,\"roleId\":101,\"admin\":false,\"params\":{},\"roleSort\":\"3\",\"deptCheckStrictly\":true,\"createBy\":\"admin\",\"menuCheckStrictly\":true,\"roleKey\":\"EPA\",\"roleName\":\"环保局\",\"deptIds\":[],\"menuIds\":[2007,2012,2013,2014,2015,2016,2008,2038,2039,2009,2040,1,100,1001,1002,1003,1004,1005,1006,1007,101,1008,1009,1010,1011,1012,102,1013,1014,1015,1016,103,1017,1018,1019,1020,104,1021,1022,1023,1024,1025,105,1026,1027,1028,1029,1030,106,1031,1032,1033,1034,1035,107,1036,1037,1038,1039,108,500,1040,1041,1042,501,1043,1044,1045,2,109,1046,1047,1048,110,1049,1050,1051,1052,1053,1054,111,112,113],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:24:50');
INSERT INTO `sys_oper_log` VALUES ('291', '用户管理', '1', 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"18712341234\",\"admin\":false,\"remark\":\"处置中心——焚烧\",\"password\":\"$2a$10$fLoXhqSI8EVQHIyngeCO6euXxJmWp6f83YZnjdXyP9RGQ1SbgVbKO\",\"postIds\":[4],\"nickName\":\"处置中心——焚烧\",\"sex\":\"0\",\"deptId\":108,\"params\":{},\"userName\":\"fenshao\",\"userId\":100,\"createBy\":\"admin\",\"roleIds\":[100],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:26:31');
INSERT INTO `sys_oper_log` VALUES ('292', '用户管理', '1', 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"17912341234\",\"admin\":false,\"remark\":\"处置中心——掩埋\",\"password\":\"$2a$10$e.rAi7H0PUEhGSplDwMkcueU/AFYG/PN36avaCk.s9bZB..JgQuNK\",\"postIds\":[4],\"nickName\":\"处置中心——掩埋\",\"sex\":\"0\",\"deptId\":109,\"params\":{},\"userName\":\"yanmai\",\"userId\":101,\"createBy\":\"admin\",\"roleIds\":[100],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:27:52');
INSERT INTO `sys_oper_log` VALUES ('293', '用户管理', '1', 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"18888888888\",\"admin\":false,\"remark\":\"石嘴山XX洗煤厂\",\"password\":\"$2a$10$Op8daxu5iGm/BqSI7aneieb7/N8Yb0RU60XsWMav249GpFJyXLS9q\",\"postIds\":[2],\"nickName\":\"石嘴山XX洗煤厂\",\"sex\":\"0\",\"deptId\":200,\"params\":{},\"userName\":\"qiye\",\"userId\":102,\"createBy\":\"admin\",\"roleIds\":[2],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:28:51');
INSERT INTO `sys_oper_log` VALUES ('294', '用户管理', '1', 'com.ruoyi.web.controller.system.SysUserController.add()', 'POST', '1', 'admin', null, '/system/user', '127.0.0.1', '内网IP', '{\"phonenumber\":\"15712341234\",\"admin\":false,\"password\":\"$2a$10$e8h1mCYvnt4YNz7IYUCsDuD6SUhyMEKoAINvgF3DmoZjZuzjqnL9W\",\"postIds\":[4],\"nickName\":\"环保局\",\"sex\":\"0\",\"deptId\":101,\"params\":{},\"userName\":\"huanbaoju\",\"userId\":103,\"createBy\":\"admin\",\"roleIds\":[101],\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:31:24');
INSERT INTO `sys_oper_log` VALUES ('295', '处理步骤配置', '1', 'com.ruoyi.system.controller.HandleStepConfigController.add()', 'POST', '1', 'fenshao', null, '/handleStep', '127.0.0.1', '内网IP', '{\"createDeptId\":108,\"createDeptName\":\"焚烧部门\",\"createUserPhone\":\"18712341234\",\"params\":{},\"handleStepList\":[{\"deptId\":108,\"step\":1},{\"deptId\":109,\"step\":2}],\"createBy\":\"fenshao\",\"createTime\":1648031555925,\"state\":\"1\",\"config\":\"[{\\\"deptId\\\":108,\\\"step\\\":1},{\\\"deptId\\\":109,\\\"step\\\":2}]\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:32:34');
INSERT INTO `sys_oper_log` VALUES ('296', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"123.1\",\"createUserPhone\":\"18888888888\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"kg\",\"notifyType\":\"0\",\"createTime\":1648031645862,\"handleDeptId\":108,\"monthly\":\"2022-03\",\"name\":\"固态危险品\",\"report\":\"0\",\"createById\":102,\"id\":8,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:34:04');
INSERT INTO `sys_oper_log` VALUES ('297', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"123.1\",\"createUserPhone\":\"18888888888\",\"updateTime\":1648031654455,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"kg\",\"notifyType\":\"0\",\"createTime\":1648031646000,\"updateBy\":\"qiye\",\"handleDeptId\":108,\"monthly\":\"2022-02\",\"name\":\"固态危险品\",\"report\":\"0\",\"createById\":102,\"id\":8,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:34:13');
INSERT INTO `sys_oper_log` VALUES ('298', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.report()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info/report/8', '127.0.0.1', '内网IP', '8', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:34:14');
INSERT INTO `sys_oper_log` VALUES ('299', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"100\",\"createUserPhone\":\"18888888888\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"kg\",\"notifyType\":\"0\",\"createTime\":1648031683305,\"handleDeptId\":108,\"monthly\":\"2022-03\",\"name\":\"实际固态危险品\",\"report\":\"0\",\"createById\":102,\"id\":9,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:34:42');
INSERT INTO `sys_oper_log` VALUES ('300', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"100\",\"createUserPhone\":\"18888888888\",\"updateTime\":1648031692842,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"kg\",\"notifyType\":\"0\",\"createTime\":1648031683000,\"updateBy\":\"qiye\",\"handleDeptId\":108,\"monthly\":\"2022-02\",\"name\":\"实际固态危险品\",\"report\":\"0\",\"createById\":102,\"id\":9,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:34:51');
INSERT INTO `sys_oper_log` VALUES ('301', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.report()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info/report/9', '127.0.0.1', '内网IP', '9', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:34:54');
INSERT INTO `sys_oper_log` VALUES ('302', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'fenshao', null, '/wasteDisposal/info/handleWaste/9', '127.0.0.1', '内网IP', '9', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:37:54');
INSERT INTO `sys_oper_log` VALUES ('303', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'yanmai', null, '/wasteDisposal/info/handleWaste/9', '127.0.0.1', '内网IP', '9', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:38:57');
INSERT INTO `sys_oper_log` VALUES ('304', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.notify()', 'POST', '1', 'huanbaoju', null, '/wasteDisposal/info/notify', '127.0.0.1', '内网IP', '{\"id\":9,\"notifyType\":\"2\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-23 18:40:16');
INSERT INTO `sys_oper_log` VALUES ('305', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"num\":\"12.5\",\"createUserPhone\":\"18888888888\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"monthly\":\"2022-04\",\"name\":\"测试固态\",\"createById\":102,\"state\":\"3\",\"plan\":\"1\"}', null, '1', '此形态下未配置处理步骤,请等待处置中心配置！', '2022-03-24 08:58:19');
INSERT INTO `sys_oper_log` VALUES ('306', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"12.5\",\"createUserPhone\":\"18888888888\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"notifyType\":\"0\",\"createTime\":1648083505620,\"handleDeptId\":108,\"monthly\":\"2022-04\",\"name\":\"测试固态\",\"report\":\"0\",\"createById\":102,\"id\":10,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 08:58:24');
INSERT INTO `sys_oper_log` VALUES ('307', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.report()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info/report/10', '127.0.0.1', '内网IP', '10', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:00:18');
INSERT INTO `sys_oper_log` VALUES ('308', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"10\",\"createUserPhone\":\"18888888888\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"notifyType\":\"0\",\"createTime\":1648083657371,\"handleDeptId\":108,\"monthly\":\"2022-10\",\"name\":\"测试对的\",\"report\":\"0\",\"createById\":102,\"id\":11,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:00:55');
INSERT INTO `sys_oper_log` VALUES ('309', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"10\",\"createUserPhone\":\"18888888888\",\"updateTime\":1648083669211,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"notifyType\":\"0\",\"createTime\":1648083657000,\"updateBy\":\"qiye\",\"handleDeptId\":108,\"monthly\":\"2022-04\",\"name\":\"测试对的\",\"report\":\"0\",\"createById\":102,\"id\":11,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:01:07');
INSERT INTO `sys_oper_log` VALUES ('310', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.report()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info/report/11', '127.0.0.1', '内网IP', '11', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:01:08');
INSERT INTO `sys_oper_log` VALUES ('311', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'fenshao', null, '/wasteDisposal/info/handleWaste/11', '127.0.0.1', '内网IP', '11', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:01:31');
INSERT INTO `sys_oper_log` VALUES ('312', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.handleWaste()', 'PUT', '1', 'yanmai', null, '/wasteDisposal/info/handleWaste/11', '127.0.0.1', '内网IP', '11', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:02:23');
INSERT INTO `sys_oper_log` VALUES ('313', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.notify()', 'POST', '1', 'huanbaoju', null, '/wasteDisposal/info/notify', '127.0.0.1', '内网IP', '{\"id\":11,\"notifyType\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:07:33');
INSERT INTO `sys_oper_log` VALUES ('314', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"num\":\"112.1\",\"createUserPhone\":\"18888888888\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"monthly\":\"2022-04\",\"name\":\"固态\",\"createById\":102,\"state\":\"1\",\"plan\":\"1\"}', null, '1', '此形态下月度上报已存在,请无重复添加！', '2022-03-24 09:28:08');
INSERT INTO `sys_oper_log` VALUES ('315', '危险废物处置信息', '1', 'com.ruoyi.system.controller.WasteDisposalInfoController.add()', 'POST', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"112.1\",\"createUserPhone\":\"18888888888\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"notifyType\":\"0\",\"createTime\":1648085299624,\"handleDeptId\":108,\"monthly\":\"2022-05\",\"name\":\"固态\",\"report\":\"0\",\"createById\":102,\"id\":12,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:28:18');
INSERT INTO `sys_oper_log` VALUES ('316', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"112.1\",\"createUserPhone\":\"18888888888\",\"updateTime\":1648085309993,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"notifyType\":\"0\",\"createTime\":1648085300000,\"updateBy\":\"qiye\",\"handleDeptId\":108,\"monthly\":\"2022-05\",\"name\":\"固态\",\"report\":\"0\",\"createById\":102,\"id\":12,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:28:28');
INSERT INTO `sys_oper_log` VALUES ('317', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"112.1\",\"createUserPhone\":\"18888888888\",\"updateTime\":1648085310000,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"notifyType\":\"0\",\"createTime\":1648085300000,\"updateBy\":\"qiye\",\"handleDeptId\":108,\"monthly\":\"2022-04\",\"name\":\"固态\",\"report\":\"0\",\"createById\":102,\"id\":12,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"1\"}', null, '1', '此形态下月度上报已存在,请无重复添加！', '2022-03-24 09:28:33');
INSERT INTO `sys_oper_log` VALUES ('318', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"112.1\",\"createUserPhone\":\"18888888888\",\"updateTime\":1648085310000,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"notifyType\":\"0\",\"createTime\":1648085300000,\"updateBy\":\"qiye\",\"handleDeptId\":108,\"monthly\":\"2022-02\",\"name\":\"固态\",\"report\":\"0\",\"createById\":102,\"id\":12,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"1\"}', null, '1', '此形态下月度上报已存在,请无重复添加！', '2022-03-24 09:28:46');
INSERT INTO `sys_oper_log` VALUES ('319', '危险废物处置信息', '2', 'com.ruoyi.system.controller.WasteDisposalInfoController.edit()', 'PUT', '1', 'qiye', null, '/wasteDisposal/info', '127.0.0.1', '内网IP', '{\"createDeptId\":200,\"createDeptName\":\"石嘴山XX洗煤厂\",\"handleStep\":1,\"num\":\"112.11\",\"createUserPhone\":\"18888888888\",\"updateTime\":1648085347400,\"delFlag\":\"0\",\"params\":{},\"createBy\":\"qiye\",\"unit\":\"k\",\"notifyType\":\"0\",\"createTime\":1648085300000,\"updateBy\":\"qiye\",\"handleDeptId\":108,\"monthly\":\"2022-05\",\"name\":\"固态\",\"report\":\"0\",\"createById\":102,\"id\":12,\"state\":\"1\",\"complete\":\"0\",\"plan\":\"1\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 09:29:05');
INSERT INTO `sys_oper_log` VALUES ('320', '字典类型', '1', 'com.ruoyi.web.controller.system.SysDictTypeController.add()', 'POST', '1', 'admin', null, '/system/dict/type', '127.0.0.1', '内网IP', '{\"createBy\":\"admin\",\"dictName\":\"所属区域\",\"remark\":\"所属区域\",\"params\":{},\"dictType\":\"dict_area_code\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 17:18:27');
INSERT INTO `sys_oper_log` VALUES ('321', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"640202000000\",\"listClass\":\"default\",\"dictSort\":0,\"params\":{},\"dictType\":\"dict_area_code\",\"dictLabel\":\"大武口区\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 17:20:08');
INSERT INTO `sys_oper_log` VALUES ('322', '字典数据', '1', 'com.ruoyi.web.controller.system.SysDictDataController.add()', 'POST', '1', 'admin', null, '/system/dict/data', '127.0.0.1', '内网IP', '{\"dictValue\":\"640205000000\",\"listClass\":\"default\",\"dictSort\":1,\"params\":{},\"dictType\":\"dict_area_code\",\"dictLabel\":\"惠农区\",\"createBy\":\"admin\",\"default\":false,\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 17:20:29');
INSERT INTO `sys_oper_log` VALUES ('323', '字典类型', '9', 'com.ruoyi.web.controller.system.SysDictTypeController.refreshCache()', 'DELETE', '1', 'admin', null, '/system/dict/type/refreshCache', '127.0.0.1', '内网IP', '{}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 17:20:35');
INSERT INTO `sys_oper_log` VALUES ('324', '部门管理', '2', 'com.ruoyi.web.controller.system.SysDeptController.edit()', 'PUT', '1', 'admin', null, '/system/dept', '127.0.0.1', '内网IP', '{\"deptName\":\"石嘴山环保局\",\"leader\":\"若依\",\"address\":\"宁夏是石嘴山\",\"deptId\":101,\"orderNum\":\"1\",\"delFlag\":\"0\",\"params\":{},\"parentId\":100,\"areaCode\":\"640205000000\",\"createBy\":\"admin\",\"children\":[],\"createTime\":1647583185000,\"phone\":\"15888888888\",\"updateBy\":\"admin\",\"organization\":\"000000001\",\"ancestors\":\"0,100\",\"representative\":\"局长\",\"email\":\"ry@qq.com\",\"status\":\"0\"}', '{\"msg\":\"操作成功\",\"code\":200}', '0', null, '2022-03-24 17:23:20');

-- ----------------------------
-- Table structure for `sys_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_post`;
CREATE TABLE `sys_post` (
  `post_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '岗位ID',
  `post_code` varchar(64) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '岗位编码',
  `post_name` varchar(50) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '岗位名称',
  `post_sort` int(4) NOT NULL COMMENT '显示顺序',
  `status` char(1) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '状态（0正常 1停用）',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`post_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='岗位信息表';

-- ----------------------------
-- Records of sys_post
-- ----------------------------
INSERT INTO `sys_post` VALUES ('1', 'ceo', '董事长', '1', '0', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_post` VALUES ('2', 'se', '项目经理', '2', '0', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_post` VALUES ('3', 'hr', '人力资源', '3', '0', 'admin', '2022-03-18 13:59:45', '', null, '');
INSERT INTO `sys_post` VALUES ('4', 'user', '普通员工', '4', '0', 'admin', '2022-03-18 13:59:45', '', null, '');

-- ----------------------------
-- Table structure for `sys_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '角色ID',
  `role_name` varchar(30) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '角色名称',
  `role_key` varchar(100) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '角色权限字符串',
  `role_sort` int(4) NOT NULL COMMENT '显示顺序',
  `data_scope` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '1' COMMENT '数据范围（1：全部数据权限 2：自定数据权限 3：本部门数据权限 4：本部门及以下数据权限）',
  `menu_check_strictly` tinyint(1) DEFAULT '1' COMMENT '菜单树选择项是否关联显示',
  `dept_check_strictly` tinyint(1) DEFAULT '1' COMMENT '部门树选择项是否关联显示',
  `status` char(1) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '角色状态（0正常 1停用）',
  `del_flag` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=102 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='角色信息表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('1', '超级管理员', 'admin', '1', '1', '1', '1', '0', '0', 'admin', '2022-03-18 13:59:45', '', null, '超级管理员');
INSERT INTO `sys_role` VALUES ('2', '企业', 'company', '2', '2', '1', '1', '0', '0', 'admin', '2022-03-18 13:59:45', 'admin', '2022-03-23 18:23:00', '企业');
INSERT INTO `sys_role` VALUES ('100', '处置中心', 'disposalCenter', '3', '1', '1', '1', '0', '0', 'admin', '2022-03-23 18:23:55', 'admin', '2022-03-23 18:24:05', '处置中心');
INSERT INTO `sys_role` VALUES ('101', '环保局', 'EPA', '3', '1', '1', '1', '0', '0', 'admin', '2022-03-23 18:24:50', '', null, null);

-- ----------------------------
-- Table structure for `sys_role_dept`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_dept`;
CREATE TABLE `sys_role_dept` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `dept_id` bigint(20) NOT NULL COMMENT '部门ID',
  PRIMARY KEY (`role_id`,`dept_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='角色和部门关联表';

-- ----------------------------
-- Records of sys_role_dept
-- ----------------------------
INSERT INTO `sys_role_dept` VALUES ('2', '100');
INSERT INTO `sys_role_dept` VALUES ('2', '101');
INSERT INTO `sys_role_dept` VALUES ('2', '105');

-- ----------------------------
-- Table structure for `sys_role_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单ID',
  PRIMARY KEY (`role_id`,`menu_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='角色和菜单关联表';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('2', '2000');
INSERT INTO `sys_role_menu` VALUES ('2', '2001');
INSERT INTO `sys_role_menu` VALUES ('2', '2006');
INSERT INTO `sys_role_menu` VALUES ('2', '2010');
INSERT INTO `sys_role_menu` VALUES ('2', '2021');
INSERT INTO `sys_role_menu` VALUES ('2', '2022');
INSERT INTO `sys_role_menu` VALUES ('2', '2023');
INSERT INTO `sys_role_menu` VALUES ('2', '2024');
INSERT INTO `sys_role_menu` VALUES ('2', '2025');
INSERT INTO `sys_role_menu` VALUES ('2', '2026');
INSERT INTO `sys_role_menu` VALUES ('2', '2027');
INSERT INTO `sys_role_menu` VALUES ('2', '2028');
INSERT INTO `sys_role_menu` VALUES ('2', '2029');
INSERT INTO `sys_role_menu` VALUES ('2', '2030');
INSERT INTO `sys_role_menu` VALUES ('100', '2002');
INSERT INTO `sys_role_menu` VALUES ('100', '2003');
INSERT INTO `sys_role_menu` VALUES ('100', '2005');
INSERT INTO `sys_role_menu` VALUES ('100', '2011');
INSERT INTO `sys_role_menu` VALUES ('100', '2017');
INSERT INTO `sys_role_menu` VALUES ('100', '2018');
INSERT INTO `sys_role_menu` VALUES ('100', '2019');
INSERT INTO `sys_role_menu` VALUES ('100', '2020');
INSERT INTO `sys_role_menu` VALUES ('100', '2031');
INSERT INTO `sys_role_menu` VALUES ('100', '2032');
INSERT INTO `sys_role_menu` VALUES ('100', '2033');
INSERT INTO `sys_role_menu` VALUES ('100', '2034');
INSERT INTO `sys_role_menu` VALUES ('100', '2035');
INSERT INTO `sys_role_menu` VALUES ('100', '2036');
INSERT INTO `sys_role_menu` VALUES ('100', '2037');
INSERT INTO `sys_role_menu` VALUES ('101', '1');
INSERT INTO `sys_role_menu` VALUES ('101', '2');
INSERT INTO `sys_role_menu` VALUES ('101', '100');
INSERT INTO `sys_role_menu` VALUES ('101', '101');
INSERT INTO `sys_role_menu` VALUES ('101', '102');
INSERT INTO `sys_role_menu` VALUES ('101', '103');
INSERT INTO `sys_role_menu` VALUES ('101', '104');
INSERT INTO `sys_role_menu` VALUES ('101', '105');
INSERT INTO `sys_role_menu` VALUES ('101', '106');
INSERT INTO `sys_role_menu` VALUES ('101', '107');
INSERT INTO `sys_role_menu` VALUES ('101', '108');
INSERT INTO `sys_role_menu` VALUES ('101', '109');
INSERT INTO `sys_role_menu` VALUES ('101', '110');
INSERT INTO `sys_role_menu` VALUES ('101', '111');
INSERT INTO `sys_role_menu` VALUES ('101', '112');
INSERT INTO `sys_role_menu` VALUES ('101', '113');
INSERT INTO `sys_role_menu` VALUES ('101', '500');
INSERT INTO `sys_role_menu` VALUES ('101', '501');
INSERT INTO `sys_role_menu` VALUES ('101', '1001');
INSERT INTO `sys_role_menu` VALUES ('101', '1002');
INSERT INTO `sys_role_menu` VALUES ('101', '1003');
INSERT INTO `sys_role_menu` VALUES ('101', '1004');
INSERT INTO `sys_role_menu` VALUES ('101', '1005');
INSERT INTO `sys_role_menu` VALUES ('101', '1006');
INSERT INTO `sys_role_menu` VALUES ('101', '1007');
INSERT INTO `sys_role_menu` VALUES ('101', '1008');
INSERT INTO `sys_role_menu` VALUES ('101', '1009');
INSERT INTO `sys_role_menu` VALUES ('101', '1010');
INSERT INTO `sys_role_menu` VALUES ('101', '1011');
INSERT INTO `sys_role_menu` VALUES ('101', '1012');
INSERT INTO `sys_role_menu` VALUES ('101', '1013');
INSERT INTO `sys_role_menu` VALUES ('101', '1014');
INSERT INTO `sys_role_menu` VALUES ('101', '1015');
INSERT INTO `sys_role_menu` VALUES ('101', '1016');
INSERT INTO `sys_role_menu` VALUES ('101', '1017');
INSERT INTO `sys_role_menu` VALUES ('101', '1018');
INSERT INTO `sys_role_menu` VALUES ('101', '1019');
INSERT INTO `sys_role_menu` VALUES ('101', '1020');
INSERT INTO `sys_role_menu` VALUES ('101', '1021');
INSERT INTO `sys_role_menu` VALUES ('101', '1022');
INSERT INTO `sys_role_menu` VALUES ('101', '1023');
INSERT INTO `sys_role_menu` VALUES ('101', '1024');
INSERT INTO `sys_role_menu` VALUES ('101', '1025');
INSERT INTO `sys_role_menu` VALUES ('101', '1026');
INSERT INTO `sys_role_menu` VALUES ('101', '1027');
INSERT INTO `sys_role_menu` VALUES ('101', '1028');
INSERT INTO `sys_role_menu` VALUES ('101', '1029');
INSERT INTO `sys_role_menu` VALUES ('101', '1030');
INSERT INTO `sys_role_menu` VALUES ('101', '1031');
INSERT INTO `sys_role_menu` VALUES ('101', '1032');
INSERT INTO `sys_role_menu` VALUES ('101', '1033');
INSERT INTO `sys_role_menu` VALUES ('101', '1034');
INSERT INTO `sys_role_menu` VALUES ('101', '1035');
INSERT INTO `sys_role_menu` VALUES ('101', '1036');
INSERT INTO `sys_role_menu` VALUES ('101', '1037');
INSERT INTO `sys_role_menu` VALUES ('101', '1038');
INSERT INTO `sys_role_menu` VALUES ('101', '1039');
INSERT INTO `sys_role_menu` VALUES ('101', '1040');
INSERT INTO `sys_role_menu` VALUES ('101', '1041');
INSERT INTO `sys_role_menu` VALUES ('101', '1042');
INSERT INTO `sys_role_menu` VALUES ('101', '1043');
INSERT INTO `sys_role_menu` VALUES ('101', '1044');
INSERT INTO `sys_role_menu` VALUES ('101', '1045');
INSERT INTO `sys_role_menu` VALUES ('101', '1046');
INSERT INTO `sys_role_menu` VALUES ('101', '1047');
INSERT INTO `sys_role_menu` VALUES ('101', '1048');
INSERT INTO `sys_role_menu` VALUES ('101', '1049');
INSERT INTO `sys_role_menu` VALUES ('101', '1050');
INSERT INTO `sys_role_menu` VALUES ('101', '1051');
INSERT INTO `sys_role_menu` VALUES ('101', '1052');
INSERT INTO `sys_role_menu` VALUES ('101', '1053');
INSERT INTO `sys_role_menu` VALUES ('101', '1054');
INSERT INTO `sys_role_menu` VALUES ('101', '2007');
INSERT INTO `sys_role_menu` VALUES ('101', '2008');
INSERT INTO `sys_role_menu` VALUES ('101', '2009');
INSERT INTO `sys_role_menu` VALUES ('101', '2012');
INSERT INTO `sys_role_menu` VALUES ('101', '2013');
INSERT INTO `sys_role_menu` VALUES ('101', '2014');
INSERT INTO `sys_role_menu` VALUES ('101', '2015');
INSERT INTO `sys_role_menu` VALUES ('101', '2016');
INSERT INTO `sys_role_menu` VALUES ('101', '2038');
INSERT INTO `sys_role_menu` VALUES ('101', '2039');
INSERT INTO `sys_role_menu` VALUES ('101', '2040');

-- ----------------------------
-- Table structure for `sys_user`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `dept_id` bigint(20) DEFAULT NULL COMMENT '部门ID',
  `user_name` varchar(30) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(30) COLLATE utf8mb4_croatian_ci NOT NULL COMMENT '用户昵称',
  `user_type` varchar(2) COLLATE utf8mb4_croatian_ci DEFAULT '00' COMMENT '用户类型（00系统用户）',
  `email` varchar(50) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '用户邮箱',
  `phonenumber` varchar(11) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '手机号码',
  `sex` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '用户性别（0男 1女 2未知）',
  `avatar` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '头像地址',
  `password` varchar(100) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '密码',
  `status` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '帐号状态（0正常 1停用）',
  `del_flag` char(1) COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `login_ip` varchar(128) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '最后登录IP',
  `login_date` datetime DEFAULT NULL COMMENT '最后登录时间',
  `create_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) COLLATE utf8mb4_croatian_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='用户信息表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('1', '103', 'admin', '超级管理员', '00', 'ry@163.com', '15888888888', '1', '', '$2a$10$7JB720yubVSZvUI0rEqK/.VqGOZTH.ulu33dHOiBE8ByOhJIrdAu2', '0', '0', '127.0.0.1', '2022-03-26 09:39:44', 'admin', '2022-03-18 13:59:45', '', '2022-03-26 09:39:37', '管理员');
INSERT INTO `sys_user` VALUES ('100', '108', 'fenshao', '处置中心——焚烧', '00', '', '18712341234', '0', '', '$2a$10$fLoXhqSI8EVQHIyngeCO6euXxJmWp6f83YZnjdXyP9RGQ1SbgVbKO', '0', '0', '127.0.0.1', '2022-03-24 09:01:22', 'admin', '2022-03-23 18:26:30', '', '2022-03-24 09:01:20', '处置中心——焚烧');
INSERT INTO `sys_user` VALUES ('101', '109', 'yanmai', '处置中心——掩埋', '00', '', '17912341234', '0', '', '$2a$10$e.rAi7H0PUEhGSplDwMkcueU/AFYG/PN36avaCk.s9bZB..JgQuNK', '0', '0', '127.0.0.1', '2022-03-24 09:52:44', 'admin', '2022-03-23 18:27:52', '', '2022-03-24 09:52:42', '处置中心——掩埋');
INSERT INTO `sys_user` VALUES ('102', '200', 'qiye', '石嘴山XX洗煤厂', '00', '', '18888888888', '0', '', '$2a$10$Op8daxu5iGm/BqSI7aneieb7/N8Yb0RU60XsWMav249GpFJyXLS9q', '0', '0', '127.0.0.1', '2022-03-24 09:08:11', 'admin', '2022-03-23 18:28:50', '', '2022-03-24 09:08:09', '石嘴山XX洗煤厂');
INSERT INTO `sys_user` VALUES ('103', '101', 'huanbaoju', '环保局', '00', '', '15712341234', '0', '', '$2a$10$e8h1mCYvnt4YNz7IYUCsDuD6SUhyMEKoAINvgF3DmoZjZuzjqnL9W', '0', '0', '127.0.0.1', '2022-03-24 18:51:12', 'admin', '2022-03-23 18:31:23', '', '2022-03-24 18:51:09', null);

-- ----------------------------
-- Table structure for `sys_user_post`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_post`;
CREATE TABLE `sys_user_post` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `post_id` bigint(20) NOT NULL COMMENT '岗位ID',
  PRIMARY KEY (`user_id`,`post_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='用户与岗位关联表';

-- ----------------------------
-- Records of sys_user_post
-- ----------------------------
INSERT INTO `sys_user_post` VALUES ('1', '1');
INSERT INTO `sys_user_post` VALUES ('100', '4');
INSERT INTO `sys_user_post` VALUES ('101', '4');
INSERT INTO `sys_user_post` VALUES ('102', '2');
INSERT INTO `sys_user_post` VALUES ('103', '4');

-- ----------------------------
-- Table structure for `sys_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` bigint(20) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_croatian_ci COMMENT='用户和角色关联表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('1', '1');
INSERT INTO `sys_user_role` VALUES ('100', '100');
INSERT INTO `sys_user_role` VALUES ('101', '100');
INSERT INTO `sys_user_role` VALUES ('102', '2');
INSERT INTO `sys_user_role` VALUES ('103', '101');

-- ----------------------------
-- Table structure for `t_handle_log`
-- ----------------------------
DROP TABLE IF EXISTS `t_handle_log`;
CREATE TABLE `t_handle_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `waste_id` bigint(20) DEFAULT NULL COMMENT '废物处理id',
  `step` int(10) DEFAULT NULL COMMENT '处理步骤',
  `create_dept_id` bigint(20) DEFAULT NULL COMMENT '创建部门id',
  `create_dept_name` varchar(30) DEFAULT NULL COMMENT '创建部门名称',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_user_phone` varchar(20) DEFAULT NULL COMMENT '创建人电话',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COMMENT='处理记录表';

-- ----------------------------
-- Records of t_handle_log
-- ----------------------------
INSERT INTO `t_handle_log` VALUES ('6', '9', '1', '108', '焚烧部门', 'fenshao', '2022-03-23 18:37:56', '18712341234', '0');
INSERT INTO `t_handle_log` VALUES ('7', '11', '1', '108', '焚烧部门', 'fenshao', '2022-03-24 09:01:33', '18712341234', '0');
INSERT INTO `t_handle_log` VALUES ('8', '11', '2', '109', '掩埋部门', 'yanmai', '2022-03-24 09:02:25', '17912341234', '0');

-- ----------------------------
-- Table structure for `t_handle_step_config`
-- ----------------------------
DROP TABLE IF EXISTS `t_handle_step_config`;
CREATE TABLE `t_handle_step_config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `state` varchar(2) DEFAULT NULL COMMENT '形态',
  `config` longtext COMMENT '配置',
  `create_dept_id` bigint(20) DEFAULT NULL COMMENT '创建部门id',
  `create_dept_name` varchar(30) DEFAULT NULL COMMENT '创建部门名称',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_phone` varchar(20) DEFAULT NULL COMMENT '创建人电话',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='处理步骤配置表';

-- ----------------------------
-- Records of t_handle_step_config
-- ----------------------------
INSERT INTO `t_handle_step_config` VALUES ('10', '1', '[{\"deptId\":108,\"step\":1},{\"deptId\":109,\"step\":2}]', '108', '焚烧部门', 'fenshao', '2022-03-23 18:32:36', '', null, '18712341234', '0');

-- ----------------------------
-- Table structure for `t_waste_disposal_info`
-- ----------------------------
DROP TABLE IF EXISTS `t_waste_disposal_info`;
CREATE TABLE `t_waste_disposal_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `num` varchar(10) DEFAULT NULL COMMENT '数量',
  `unit` varchar(2) DEFAULT NULL COMMENT '单位',
  `plan` varchar(2) DEFAULT NULL COMMENT '是否计划',
  `state` varchar(2) DEFAULT NULL COMMENT '形态',
  `monthly` varchar(10) DEFAULT NULL COMMENT '月份',
  `complete` varchar(2) DEFAULT NULL COMMENT '是否完成处理',
  `report` varchar(2) DEFAULT NULL COMMENT '上报状态',
  `create_dept_id` bigint(20) DEFAULT NULL COMMENT '创建部门id',
  `create_dept_name` varchar(30) DEFAULT NULL COMMENT '创建部门名称',
  `create_by_id` bigint(20) DEFAULT NULL COMMENT '创建者id',
  `create_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '创建者',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT '' COMMENT '更新者',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `create_user_phone` varchar(20) DEFAULT NULL COMMENT '创建人电话',
  `handle_step` int(10) DEFAULT NULL COMMENT '当前处理步骤',
  `handle_dept_id` bigint(20) DEFAULT NULL COMMENT '当前处理部门id',
  `del_flag` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_croatian_ci DEFAULT '0' COMMENT '删除标志（0代表存在 2代表删除）',
  `notify_type` char(2) DEFAULT NULL COMMENT '通报状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COMMENT='危险废物处置信息表';

-- ----------------------------
-- Records of t_waste_disposal_info
-- ----------------------------
INSERT INTO `t_waste_disposal_info` VALUES ('8', '固态危险品', '123.1', 'kg', '1', '1', '2022-02', '0', '1', '200', '石嘴山XX洗煤厂', '102', 'qiye', '2022-03-23 18:34:06', 'qiye', '2022-03-23 18:34:14', '18888888888', '1', '108', '0', '0');
INSERT INTO `t_waste_disposal_info` VALUES ('9', '实际固态危险品', '100', 'kg', '0', '1', '2022-02', '1', '1', '200', '石嘴山XX洗煤厂', '102', 'qiye', '2022-03-23 18:34:43', 'qiye', '2022-03-23 18:34:53', '18888888888', '2', '109', '0', '2');
INSERT INTO `t_waste_disposal_info` VALUES ('10', '测试固态', '12.5', 'k', '1', '1', '2022-04', '0', '1', '200', '石嘴山XX洗煤厂', '102', 'qiye', '2022-03-24 08:58:26', '', null, '18888888888', '1', '108', '0', '0');
INSERT INTO `t_waste_disposal_info` VALUES ('11', '测试对的', '10', 'k', '0', '1', '2022-04', '1', '1', '200', '石嘴山XX洗煤厂', '102', 'qiye', '2022-03-24 09:00:57', 'qiye', '2022-03-24 09:01:09', '18888888888', '2', '109', '0', '1');
INSERT INTO `t_waste_disposal_info` VALUES ('12', '固态', '112.11', 'k', '1', '1', '2022-05', '0', '0', '200', '石嘴山XX洗煤厂', '102', 'qiye', '2022-03-24 09:28:20', 'qiye', '2022-03-24 09:29:07', '18888888888', '1', '108', '0', '0');
